<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('remember_token');
            $table->string('password', 60);
            $table->string('phone');
            $table->string('address');
            $table->string('city');
            $table->string('district');
            $table->string('state');
            $table->string('pin');
            $table->string('usertype');
            $table->boolean('isAdmin');
            $table->string('permission');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
