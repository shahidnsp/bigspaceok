<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->string('price');
            $table->string('status');
            $table->string('monthorannual');
            $table->string('area');
            $table->string('address');
            $table->string('city');
            $table->string('district_id');
            $table->string('state_id');
            $table->string('pincode');
            $table->string('description');
            $table->string('contactname');
            $table->string('email');
            $table->string('phone');
            $table->integer('approve')->default(0);
            $table->boolean('isSale');
            $table->integer('latest_deals_id');
            $table->integer('property_type_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
