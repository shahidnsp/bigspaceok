<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'phone'=>$faker->phoneNumber,
        'address'=>$faker->address,
        'city'=>$faker->city,
        'district'=>$faker->city,
        'state'=>$faker->city,
        'pin'=>'676221',
        'usertype'=>'Admin',
        'isAdmin'=>'1',
        'permission'=>'1111,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000',
        'password' => 'admin'
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'logo' => 'logo.png',
    ];
});

$factory->define(App\FeatureList::class, function (Faker\Generator $faker) {
    return [

        'property_id' => $faker->randomElement([1,2,3,4,5]),
        'property_feature_id' => $faker->randomElement([1,2,3,4,5]),

    ];
});

$factory->define(App\LatestDeal::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'place' => $faker->city,
        'description' => $faker->sentence,
    ];
});

$factory->define(App\Photo::class, function (Faker\Generator $faker) {
    return [
        'name' => 'dummydeals.jpg',
        'temp' => 'dummydeals.jpg',
        'property_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'latest_deal_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});

$factory->define(App\PropertyFeature::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(),
    ];
});

$factory->define(App\PropertyType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});

$factory->define(App\Property::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'location' => $faker->randomElement(['Manjeri','Kottakkal','Nilambur']),
        'price' => $faker->randomElement([12,34,75]),
        'status' => $faker->randomElement(['For Rent','For Sale']),
        'monthorannual' => $faker->randomElement(['Month','Year']),
        'area' => $faker->name,
        'address' => $faker->sentence,
        'city' => $faker->name,
        'district_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'state_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'pincode' => $faker->randomElement([676508,678535]),
        'description' => $faker->sentence,
        'contactname' => $faker->name,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'approve' => $faker->randomElement([1,0]),
        'isSale' => $faker->randomElement([0,1]),
        'latest_deals_id' => $faker->sentence,
        'property_type_id' => $faker->sentence,
        'user_id' => $faker->sentence,
    ];
});


$factory->define(App\Testimonial::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'designation' => $faker->name,
        'content' => $faker->sentence,
        'photo' => 'avatar.jpg',
    ];
});



$factory->define(App\Blog::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(100),
        'photo' => 'dummydeals.jpg',
    ];
});
$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'subject' => $faker->randomElement(['sub1','sub2']),
        'phone' => $faker->phoneNumber,
        'message' => $faker->name,
    ];
});

$factory->define(App\FeaturedProperty::class, function (Faker\Generator $faker) {
    return [
        'property_id' => $faker->randomElement([2,1]),
        'description' => $faker->sentence,
        'order' => $faker->randomElement([0,1]),

    ];
});

$factory->define(App\GetQuote::class, function (Faker\Generator $faker) {
    return [
        'property_id' => $faker->randomElement([1,2,3,4,5]),
        'name' => $faker->name,
        'email' => $faker->email,
        'number1' => $faker->phoneNumber,
        'number2' => $faker->phoneNumber,
        'place' => $faker->city,
    ];
});

$factory->define(App\Gallery::class, function (Faker\Generator $faker) {
    return [
        'photo' => 'dummydeals.jpg',
        'temp' => 'temp.jpg',
    ];
});

$factory->define(App\Moment::class, function (Faker\Generator $faker) {
    return [
        'photo' => 'dummydeals.jpg',
    ];
});




