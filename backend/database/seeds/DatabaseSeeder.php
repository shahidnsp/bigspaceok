<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();

        \App\User::create([
            'email'=>'admin@admin.com',
            'password'=>'admin',
            'name'=>'Admin',
            'phone'=>'99999999999',
            'address'=>'',
            'city'=>'',
            'district'=>'',
            'state'=>'',
            'pin'=>'',
            'usertype'=>'Admin',
            'isAdmin'=>'1',
            'permission'=>'1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111']);

        \App\User::create([
            'email'=>'user@admin.com',
            'password'=>'admin',
            'name'=>'Admin',
            'phone'=>'99999999999',
            'address'=>'',
            'city'=>'',
            'district'=>'',
            'state'=>'',
            'pin'=>'',
            'usertype'=>'User',
            'isAdmin'=>'1',
            'permission'=>'0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000']);

        // $this->call(UserTableSeeder::class);

        //STATES SEEDING START
        \App\State::create(['name'=>'Andhra Pradesh']);
        \App\State::create(['name'=>'Arunachal Pradesh']);
        \App\State::create(['name'=>'Assam']);
        \App\State::create(['name'=>'Bihar']);
        \App\State::create(['name'=>'Chhattisgarh']);
        \App\State::create(['name'=>'Goa']);
        \App\State::create(['name'=>'Gujarat']);
        \App\State::create(['name'=>'Haryana']);
        \App\State::create(['name'=>'Himachal Pradesh']);
        \App\State::create(['name'=>'Jammu and Kashmir']);
        \App\State::create(['name'=>'Jharkhand']);
        \App\State::create(['name'=>'Karnataka']);
        \App\State::create(['name'=>'Kerala']);
        \App\State::create(['name'=>'Madhya Pradesh']);
        \App\State::create(['name'=>'Maharashtra']);
        \App\State::create(['name'=>'Manipur']);
        \App\State::create(['name'=>'Meghalaya']);
        \App\State::create(['name'=>'Mizoram']);
        \App\State::create(['name'=>'Nagaland']);
        \App\State::create(['name'=>'Odisha']);
        \App\State::create(['name'=>'Punjab']);
        \App\State::create(['name'=>'Rajasthan']);
        \App\State::create(['name'=>'Sikkim']);
        \App\State::create(['name'=>'Tamil Nadu']);
        \App\State::create(['name'=>'Tripura']);
        \App\State::create(['name'=>'Uttar Pradesh']);
        \App\State::create(['name'=>'Uttarakhand']);
        \App\State::create(['name'=>'West Bengal']);
        \App\State::create(['name'=>'Telangana']);
        \App\State::create(['name'=>'Andaman and Nicobar']);
        \App\State::create(['name'=>'Chandigarh']);
        \App\State::create(['name'=>'Dadra and Nagar Haveli']);
        \App\State::create(['name'=>'Daman and Diu']);
        \App\State::create(['name'=>'Lakshadweep']);
        \App\State::create(['name'=>'NCT Delhi']);
        \App\State::create(['name'=>'Puducherry']);
        //STATES SEEDING END

        //DISTRICT SEEDING START

        //Andaman and Nicobar
        \App\District::create(['state_id'=>30,'name'=>'Nicobar']);
        \App\District::create(['state_id'=>30,'name'=>'North and Middle Andaman']);
        \App\District::create(['state_id'=>30,'name'=>'South Andaman']);

        //Andhra Pradesh
        \App\District::create(['state_id'=>1,'name'=>'Anantapur']);
        \App\District::create(['state_id'=>1,'name'=>'Chittoor']);
        \App\District::create(['state_id'=>1,'name'=>'East Godavari']);
        \App\District::create(['state_id'=>1,'name'=>'Guntur']);
        \App\District::create(['state_id'=>1,'name'=>'Kadapa']);
        \App\District::create(['state_id'=>1,'name'=>'Krishna']);
        \App\District::create(['state_id'=>1,'name'=>'Kurnool']);
        \App\District::create(['state_id'=>1,'name'=>'Prakasam']);
        \App\District::create(['state_id'=>1,'name'=>'Sri Potti Sriramulu Nellore']);
        \App\District::create(['state_id'=>1,'name'=>'Srikakulam']);
        \App\District::create(['state_id'=>1,'name'=>'Visakhapatnam']);
        \App\District::create(['state_id'=>1,'name'=>'Vizianagaram']);
        \App\District::create(['state_id'=>1,'name'=>'West Godavari']);

        //Arunachal Pradesh
        \App\District::create(['state_id'=>2,'name'=>'Anjaw']);
        \App\District::create(['state_id'=>2,'name'=>'Changlang']);
        \App\District::create(['state_id'=>2,'name'=>'Dibang Valley']);
        \App\District::create(['state_id'=>2,'name'=>'East Kameng']);
        \App\District::create(['state_id'=>2,'name'=>'East Siang']);
        \App\District::create(['state_id'=>2,'name'=>'Kra Daadi']);
        \App\District::create(['state_id'=>2,'name'=>'Kurung Kumey']);
        \App\District::create(['state_id'=>2,'name'=>'Lohit']);
        \App\District::create(['state_id'=>2,'name'=>'Longding']);
        \App\District::create(['state_id'=>2,'name'=>'Lower Dibang Valley']);
        \App\District::create(['state_id'=>2,'name'=>'Lower Subansiri']);
        \App\District::create(['state_id'=>2,'name'=>'Namsai']);
        \App\District::create(['state_id'=>2,'name'=>'Papum Pare']);
        \App\District::create(['state_id'=>2,'name'=>'Siang']);
        \App\District::create(['state_id'=>2,'name'=>'Tawang']);
        \App\District::create(['state_id'=>2,'name'=>'Tirap']);
        \App\District::create(['state_id'=>2,'name'=>'Upper Siang']);
        \App\District::create(['state_id'=>2,'name'=>'Upper Subansiri']);
        \App\District::create(['state_id'=>2,'name'=>'West Kameng']);
        \App\District::create(['state_id'=>2,'name'=>'West Siang']);

        //Assam
        \App\District::create(['state_id'=>3,'name'=>'Baksa']);
        \App\District::create(['state_id'=>3,'name'=>'Barpeta']);
        \App\District::create(['state_id'=>3,'name'=>'Bishwanath']);
        \App\District::create(['state_id'=>3,'name'=>'Bongaigaon']);
        \App\District::create(['state_id'=>3,'name'=>'Cachar']);
        \App\District::create(['state_id'=>3,'name'=>'Charaideo']);
        \App\District::create(['state_id'=>3,'name'=>'Chirang']);
        \App\District::create(['state_id'=>3,'name'=>'Darrang']);
        \App\District::create(['state_id'=>3,'name'=>'Dhemaji']);
        \App\District::create(['state_id'=>3,'name'=>'Dhubri']);
        \App\District::create(['state_id'=>3,'name'=>'Dibrugarh']);
        \App\District::create(['state_id'=>3,'name'=>'Dima Hasao']);
        \App\District::create(['state_id'=>3,'name'=>'Goalpara']);
        \App\District::create(['state_id'=>3,'name'=>'Golaghat']);
        \App\District::create(['state_id'=>3,'name'=>'Hailakandi']);
        \App\District::create(['state_id'=>3,'name'=>'Hojai']);
        \App\District::create(['state_id'=>3,'name'=>'Jorhat']);
        \App\District::create(['state_id'=>3,'name'=>'Kamrup']);
        \App\District::create(['state_id'=>3,'name'=>'Kamrup Metropolitan']);
        \App\District::create(['state_id'=>3,'name'=>'Karbi Anglong']);
        \App\District::create(['state_id'=>3,'name'=>'Karimganj']);
        \App\District::create(['state_id'=>3,'name'=>'Kokrajhar']);
        \App\District::create(['state_id'=>3,'name'=>'Lakhimpur']);
        \App\District::create(['state_id'=>3,'name'=>'Majuli']);
        \App\District::create(['state_id'=>3,'name'=>'Morigaon']);
        \App\District::create(['state_id'=>3,'name'=>'Nagaon']);
        \App\District::create(['state_id'=>3,'name'=>'Nalbari']);
        \App\District::create(['state_id'=>3,'name'=>'Sivasagar']);
        \App\District::create(['state_id'=>3,'name'=>'South Salmara-Mankachar']);
        \App\District::create(['state_id'=>3,'name'=>'Sonitpur']);
        \App\District::create(['state_id'=>3,'name'=>'Tinsukia']);
        \App\District::create(['state_id'=>3,'name'=>'Udalguri']);
        \App\District::create(['state_id'=>3,'name'=>'West Karbi Anglong']);

        //Bihar
        \App\District::create(['state_id'=>4,'name'=>'Araria']);
        \App\District::create(['state_id'=>4,'name'=>'Arwal']);
        \App\District::create(['state_id'=>4,'name'=>'Aurangabad']);
        \App\District::create(['state_id'=>4,'name'=>'Banka']);
        \App\District::create(['state_id'=>4,'name'=>'Begusarai']);
        \App\District::create(['state_id'=>4,'name'=>'Bhagalpur']);
        \App\District::create(['state_id'=>4,'name'=>'Bhojpur']);
        \App\District::create(['state_id'=>4,'name'=>'Buxar']);
        \App\District::create(['state_id'=>4,'name'=>'Darbhanga']);
        \App\District::create(['state_id'=>4,'name'=>'East Champaran']);
        \App\District::create(['state_id'=>4,'name'=>'Gaya']);
        \App\District::create(['state_id'=>4,'name'=>'Gopalganj']);
        \App\District::create(['state_id'=>4,'name'=>'Jamui']);
        \App\District::create(['state_id'=>4,'name'=>'Jehanabad']);
        \App\District::create(['state_id'=>4,'name'=>'Kaimur']);
        \App\District::create(['state_id'=>4,'name'=>'Katihar']);
        \App\District::create(['state_id'=>4,'name'=>'Khagaria']);
        \App\District::create(['state_id'=>4,'name'=>'Kishanganj']);
        \App\District::create(['state_id'=>4,'name'=>'Lakhisarai']);
        \App\District::create(['state_id'=>4,'name'=>'Madhepura']);
        \App\District::create(['state_id'=>4,'name'=>'Madhubani']);
        \App\District::create(['state_id'=>4,'name'=>'Munger']);
        \App\District::create(['state_id'=>4,'name'=>'Muzaffarpur']);
        \App\District::create(['state_id'=>4,'name'=>'Nalanda']);
        \App\District::create(['state_id'=>4,'name'=>'Nawada']);
        \App\District::create(['state_id'=>4,'name'=>'Patna']);
        \App\District::create(['state_id'=>4,'name'=>'Purnia']);
        \App\District::create(['state_id'=>4,'name'=>'Rohtas']);
        \App\District::create(['state_id'=>4,'name'=>'Saharsa']);
        \App\District::create(['state_id'=>4,'name'=>'Samastipur']);
        \App\District::create(['state_id'=>4,'name'=>'Saran']);
        \App\District::create(['state_id'=>4,'name'=>'Sheikhpura']);
        \App\District::create(['state_id'=>4,'name'=>'Sheohar']);
        \App\District::create(['state_id'=>4,'name'=>'Sitamarhi']);
        \App\District::create(['state_id'=>4,'name'=>'Siwan']);
        \App\District::create(['state_id'=>4,'name'=>'Supaul']);
        \App\District::create(['state_id'=>4,'name'=>'Vaishali']);
        \App\District::create(['state_id'=>4,'name'=>'West Champaran']);

        //Chandigarh
        \App\District::create(['state_id'=>31,'name'=>'Chandigarh']);

        //Chhattisgarh
        \App\District::create(['state_id'=>5,'name'=>'Balod']);
        \App\District::create(['state_id'=>5,'name'=>'Baloda Bazar']);
        \App\District::create(['state_id'=>5,'name'=>'Balrampur']);
        \App\District::create(['state_id'=>5,'name'=>'Bastar']);
        \App\District::create(['state_id'=>5,'name'=>'Bemetara']);
        \App\District::create(['state_id'=>5,'name'=>'Bijapur']);
        \App\District::create(['state_id'=>5,'name'=>'Bilaspur']);
        \App\District::create(['state_id'=>5,'name'=>'Dantewada']);
        \App\District::create(['state_id'=>5,'name'=>'Dhamtari']);
        \App\District::create(['state_id'=>5,'name'=>'Durg']);
        \App\District::create(['state_id'=>5,'name'=>'Gariaband']);
        \App\District::create(['state_id'=>5,'name'=>'Janjgir-Champa']);
        \App\District::create(['state_id'=>5,'name'=>'Jashpur']);
        \App\District::create(['state_id'=>5,'name'=>'Kabirdham']);
        \App\District::create(['state_id'=>5,'name'=>'Kanker']);
        \App\District::create(['state_id'=>5,'name'=>'Kondagaon']);
        \App\District::create(['state_id'=>5,'name'=>'Korba']);
        \App\District::create(['state_id'=>5,'name'=>'Koriya']);
        \App\District::create(['state_id'=>5,'name'=>'Mahasamund']);
        \App\District::create(['state_id'=>5,'name'=>'Mungeli']);
        \App\District::create(['state_id'=>5,'name'=>'Narayanpur']);
        \App\District::create(['state_id'=>5,'name'=>'Raigarh']);
        \App\District::create(['state_id'=>5,'name'=>'Raipur']);
        \App\District::create(['state_id'=>5,'name'=>'Rajnandgaon']);
        \App\District::create(['state_id'=>5,'name'=>'Sukma']);
        \App\District::create(['state_id'=>5,'name'=>'Surajpur']);
        \App\District::create(['state_id'=>5,'name'=>'Surguja']);

        //Dadra and Nagar Haveli
        \App\District::create(['state_id'=>32,'name'=>'Dadra and Nagar Haveli']);

        //Daman and Diu
        \App\District::create(['state_id'=>33,'name'=>'Daman']);
        \App\District::create(['state_id'=>33,'name'=>'Diu']);

        //Delhi
        \App\District::create(['state_id'=>35,'name'=>'Central Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'East Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'New Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'North Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'North East Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'North West Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'Shahdara']);
        \App\District::create(['state_id'=>35,'name'=>'South Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'South East Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'South West Delhi']);
        \App\District::create(['state_id'=>35,'name'=>'West Delhi']);

        //Goa
        \App\District::create(['state_id'=>6,'name'=>'North Goa']);
        \App\District::create(['state_id'=>6,'name'=>'South Goa']);

        //Gujarat
        \App\District::create(['state_id'=>7,'name'=>'Ahmedabad']);
        \App\District::create(['state_id'=>7,'name'=>'Amreli']);
        \App\District::create(['state_id'=>7,'name'=>'Anand']);
        \App\District::create(['state_id'=>7,'name'=>'Aravalli']);
        \App\District::create(['state_id'=>7,'name'=>'Banaskantha']);
        \App\District::create(['state_id'=>7,'name'=>'Bharuch']);
        \App\District::create(['state_id'=>7,'name'=>'Bhavnagar']);
        \App\District::create(['state_id'=>7,'name'=>'Botad']);
        \App\District::create(['state_id'=>7,'name'=>'Chhota Udaipur']);
        \App\District::create(['state_id'=>7,'name'=>'Dahod']);
        \App\District::create(['state_id'=>7,'name'=>'Dang']);
        \App\District::create(['state_id'=>7,'name'=>'Devbhoomi Dwarka']);
        \App\District::create(['state_id'=>7,'name'=>'Gandhinagar']);
        \App\District::create(['state_id'=>7,'name'=>'Gir Somnath']);
        \App\District::create(['state_id'=>7,'name'=>'Jamnagar']);
        \App\District::create(['state_id'=>7,'name'=>'Junagadh']);
        \App\District::create(['state_id'=>7,'name'=>'Kheda']);
        \App\District::create(['state_id'=>7,'name'=>'Kutch']);
        \App\District::create(['state_id'=>7,'name'=>'Mahisagar']);
        \App\District::create(['state_id'=>7,'name'=>'Mehsana']);
        \App\District::create(['state_id'=>7,'name'=>'Morbi']);
        \App\District::create(['state_id'=>7,'name'=>'Narmada']);
        \App\District::create(['state_id'=>7,'name'=>'Navsari']);
        \App\District::create(['state_id'=>7,'name'=>'Panchmahal']);
        \App\District::create(['state_id'=>7,'name'=>'Patan']);
        \App\District::create(['state_id'=>7,'name'=>'Porbandar']);
        \App\District::create(['state_id'=>7,'name'=>'Rajkot']);
        \App\District::create(['state_id'=>7,'name'=>'Sabarkantha']);
        \App\District::create(['state_id'=>7,'name'=>'Surat']);
        \App\District::create(['state_id'=>7,'name'=>'Surendranagar']);
        \App\District::create(['state_id'=>7,'name'=>'Tapi']);
        \App\District::create(['state_id'=>7,'name'=>'Vadodara']);
        \App\District::create(['state_id'=>7,'name'=>'Valsad']);

        //Haryana
        \App\District::create(['state_id'=>8,'name'=>'Ambala']);
        \App\District::create(['state_id'=>8,'name'=>'Bhiwani']);
        \App\District::create(['state_id'=>8,'name'=>'Charkhi Dadri']);
        \App\District::create(['state_id'=>8,'name'=>'Faridabad']);
        \App\District::create(['state_id'=>8,'name'=>'Fatehabad']);
        \App\District::create(['state_id'=>8,'name'=>'Gurgaon']);
        \App\District::create(['state_id'=>8,'name'=>'Hissar']);
        \App\District::create(['state_id'=>8,'name'=>'Jhajjar']);
        \App\District::create(['state_id'=>8,'name'=>'Jind']);
        \App\District::create(['state_id'=>8,'name'=>'Kaithal']);
        \App\District::create(['state_id'=>8,'name'=>'Karnal']);
        \App\District::create(['state_id'=>8,'name'=>'Kurukshetra']);
        \App\District::create(['state_id'=>8,'name'=>'Mahendragarh']);
        \App\District::create(['state_id'=>8,'name'=>'Nuh']);
        \App\District::create(['state_id'=>8,'name'=>'Palwal']);
        \App\District::create(['state_id'=>8,'name'=>'Panchkula']);
        \App\District::create(['state_id'=>8,'name'=>'Panipat']);
        \App\District::create(['state_id'=>8,'name'=>'Rewari']);
        \App\District::create(['state_id'=>8,'name'=>'Rohtak']);
        \App\District::create(['state_id'=>8,'name'=>'Sirsa']);
        \App\District::create(['state_id'=>8,'name'=>'Sonipat']);
        \App\District::create(['state_id'=>8,'name'=>'Yamuna Nagar']);

        //Himachal Pradesh
        \App\District::create(['state_id'=>9,'name'=>'Bilaspur']);
        \App\District::create(['state_id'=>9,'name'=>'Chamba']);
        \App\District::create(['state_id'=>9,'name'=>'Hamirpur']);
        \App\District::create(['state_id'=>9,'name'=>'Kangra']);
        \App\District::create(['state_id'=>9,'name'=>'Kinnaur']);
        \App\District::create(['state_id'=>9,'name'=>'Kullu']);
        \App\District::create(['state_id'=>9,'name'=>'Lahaul and Spiti']);
        \App\District::create(['state_id'=>9,'name'=>'Mandi']);
        \App\District::create(['state_id'=>9,'name'=>'Shimla']);
        \App\District::create(['state_id'=>9,'name'=>'Sirmaur']);
        \App\District::create(['state_id'=>9,'name'=>'Solan']);
        \App\District::create(['state_id'=>9,'name'=>'Una']);

        //Jammu and Kashmir
        \App\District::create(['state_id'=>10,'name'=>'Anantnag']);
        \App\District::create(['state_id'=>10,'name'=>'Bandipora']);
        \App\District::create(['state_id'=>10,'name'=>'Baramulla']);
        \App\District::create(['state_id'=>10,'name'=>'Badgam']);
        \App\District::create(['state_id'=>10,'name'=>'Doda']);
        \App\District::create(['state_id'=>10,'name'=>'Ganderbal']);
        \App\District::create(['state_id'=>10,'name'=>'Jammu']);
        \App\District::create(['state_id'=>10,'name'=>'Kargil']);
        \App\District::create(['state_id'=>10,'name'=>'Kathua']);
        \App\District::create(['state_id'=>10,'name'=>'Kishtwar']);
        \App\District::create(['state_id'=>10,'name'=>'Kulgam']);
        \App\District::create(['state_id'=>10,'name'=>'Kupwara']);
        \App\District::create(['state_id'=>10,'name'=>'Leh']);
        \App\District::create(['state_id'=>10,'name'=>'Poonch']);
        \App\District::create(['state_id'=>10,'name'=>'Pulwama']);
        \App\District::create(['state_id'=>10,'name'=>'Rajouri']);
        \App\District::create(['state_id'=>10,'name'=>'Ramban']);
        \App\District::create(['state_id'=>10,'name'=>'Reasi']);
        \App\District::create(['state_id'=>10,'name'=>'Samba']);
        \App\District::create(['state_id'=>10,'name'=>'Shopian']);
        \App\District::create(['state_id'=>10,'name'=>'Srinagar']);
        \App\District::create(['state_id'=>10,'name'=>'Udhampur']);

        //Jharkhand
        \App\District::create(['state_id'=>11,'name'=>'Bokaro']);
        \App\District::create(['state_id'=>11,'name'=>'Chatra']);
        \App\District::create(['state_id'=>11,'name'=>'Deoghar']);
        \App\District::create(['state_id'=>11,'name'=>'Dhanbad']);
        \App\District::create(['state_id'=>11,'name'=>'Dumka']);
        \App\District::create(['state_id'=>11,'name'=>'East Singhbhum']);
        \App\District::create(['state_id'=>11,'name'=>'Garhwa']);
        \App\District::create(['state_id'=>11,'name'=>'Giridih']);
        \App\District::create(['state_id'=>11,'name'=>'Godda']);
        \App\District::create(['state_id'=>11,'name'=>'Gumla']);
        \App\District::create(['state_id'=>11,'name'=>'Hazaribag']);
        \App\District::create(['state_id'=>11,'name'=>'Jamtara']);
        \App\District::create(['state_id'=>11,'name'=>'Khunti']);
        \App\District::create(['state_id'=>11,'name'=>'Koderma']);
        \App\District::create(['state_id'=>11,'name'=>'Latehar']);
        \App\District::create(['state_id'=>11,'name'=>'Lohardaga']);
        \App\District::create(['state_id'=>11,'name'=>'Pakur']);
        \App\District::create(['state_id'=>11,'name'=>'Palamu']);
        \App\District::create(['state_id'=>11,'name'=>'Ramgarh']);
        \App\District::create(['state_id'=>11,'name'=>'Ranchi']);
        \App\District::create(['state_id'=>11,'name'=>'Sahibganj']);
        \App\District::create(['state_id'=>11,'name'=>'Seraikela Kharsawan']);
        \App\District::create(['state_id'=>11,'name'=>'Simdega']);
        \App\District::create(['state_id'=>11,'name'=>'West Singhbhum']);

        //Karnataka
        \App\District::create(['state_id'=>12,'name'=>'Bagalkot']);
        \App\District::create(['state_id'=>12,'name'=>'Ballari']);
        \App\District::create(['state_id'=>12,'name'=>'Belagavi']);
        \App\District::create(['state_id'=>12,'name'=>'Bengaluru Rural']);
        \App\District::create(['state_id'=>12,'name'=>'Bengaluru Urban']);
        \App\District::create(['state_id'=>12,'name'=>'Bidar']);
        \App\District::create(['state_id'=>12,'name'=>'Chamarajnagar']);
        \App\District::create(['state_id'=>12,'name'=>'Chikkaballapur']);
        \App\District::create(['state_id'=>12,'name'=>'Chikkamagaluru']);
        \App\District::create(['state_id'=>12,'name'=>'Chitradurga']);
        \App\District::create(['state_id'=>12,'name'=>'Dakshina Kannada']);
        \App\District::create(['state_id'=>12,'name'=>'Davanagere']);
        \App\District::create(['state_id'=>12,'name'=>'Dharwad']);
        \App\District::create(['state_id'=>12,'name'=>'Gadag']);
        \App\District::create(['state_id'=>12,'name'=>'Hassan']);
        \App\District::create(['state_id'=>12,'name'=>'Haveri']);
        \App\District::create(['state_id'=>12,'name'=>'Kalaburagi']);
        \App\District::create(['state_id'=>12,'name'=>'Kodagu']);
        \App\District::create(['state_id'=>12,'name'=>'Kolar']);
        \App\District::create(['state_id'=>12,'name'=>'Koppal']);
        \App\District::create(['state_id'=>12,'name'=>'Mandya']);
        \App\District::create(['state_id'=>12,'name'=>'Mysuru']);
        \App\District::create(['state_id'=>12,'name'=>'Raichur']);
        \App\District::create(['state_id'=>12,'name'=>'Ramanagara']);
        \App\District::create(['state_id'=>12,'name'=>'Shivamogga']);
        \App\District::create(['state_id'=>12,'name'=>'Tumakuru']);
        \App\District::create(['state_id'=>12,'name'=>'Udupi']);
        \App\District::create(['state_id'=>12,'name'=>'Uttara Kannada']);
        \App\District::create(['state_id'=>12,'name'=>'Vijayapura']);
        \App\District::create(['state_id'=>12,'name'=>'Yadgir']);

        //Kerala
        \App\District::create(['state_id'=>13,'name'=>'Alappuzha']);
        \App\District::create(['state_id'=>13,'name'=>'Ernakulam']);
        \App\District::create(['state_id'=>13,'name'=>'Idukki']);
        \App\District::create(['state_id'=>13,'name'=>'Kannur']);
        \App\District::create(['state_id'=>13,'name'=>'Kasaragod']);
        \App\District::create(['state_id'=>13,'name'=>'Kollam']);
        \App\District::create(['state_id'=>13,'name'=>'Kottayam']);
        \App\District::create(['state_id'=>13,'name'=>'Kozhikode']);
        \App\District::create(['state_id'=>13,'name'=>'Malappuram']);
        \App\District::create(['state_id'=>13,'name'=>'Palakkad']);
        \App\District::create(['state_id'=>13,'name'=>'Pathanamthitta']);
        \App\District::create(['state_id'=>13,'name'=>'Thrissur']);
        \App\District::create(['state_id'=>13,'name'=>'Thiruvananthapuram']);
        \App\District::create(['state_id'=>13,'name'=>'Wayanad']);

        //Lakshadweep
        \App\District::create(['state_id'=>34,'name'=>'Lakshadweep']);

        //Madhya Pradesh
        \App\District::create(['state_id'=>14,'name'=>'Agar Malwa']);
        \App\District::create(['state_id'=>14,'name'=>'Alirajpur']);
        \App\District::create(['state_id'=>14,'name'=>'Anuppur']);
        \App\District::create(['state_id'=>14,'name'=>'Ashok Nagar']);
        \App\District::create(['state_id'=>14,'name'=>'Balaghat']);
        \App\District::create(['state_id'=>14,'name'=>'Barwani']);
        \App\District::create(['state_id'=>14,'name'=>'Betul']);
        \App\District::create(['state_id'=>14,'name'=>'Bhind']);
        \App\District::create(['state_id'=>14,'name'=>'Bhopal']);
        \App\District::create(['state_id'=>14,'name'=>'Burhanpur']);
        \App\District::create(['state_id'=>14,'name'=>'Chhatarpur']);
        \App\District::create(['state_id'=>14,'name'=>'Chhindwara']);
        \App\District::create(['state_id'=>14,'name'=>'Damoh']);
        \App\District::create(['state_id'=>14,'name'=>'Datia']);
        \App\District::create(['state_id'=>14,'name'=>'Dewas']);
        \App\District::create(['state_id'=>14,'name'=>'Dhar']);
        \App\District::create(['state_id'=>14,'name'=>'Dindori']);
        \App\District::create(['state_id'=>14,'name'=>'Guna']);
        \App\District::create(['state_id'=>14,'name'=>'Gwalior']);
        \App\District::create(['state_id'=>14,'name'=>'Harda']);
        \App\District::create(['state_id'=>14,'name'=>'Hoshangabad']);
        \App\District::create(['state_id'=>14,'name'=>'Indore']);
        \App\District::create(['state_id'=>14,'name'=>'Jabalpur']);
        \App\District::create(['state_id'=>14,'name'=>'Jhabua']);
        \App\District::create(['state_id'=>14,'name'=>'Katni']);
        \App\District::create(['state_id'=>14,'name'=>'Khandwa (East Nimar)']);
        \App\District::create(['state_id'=>14,'name'=>'Khargone (West Nimar)']);
        \App\District::create(['state_id'=>14,'name'=>'Mandla']);
        \App\District::create(['state_id'=>14,'name'=>'Mandsaur']);
        \App\District::create(['state_id'=>14,'name'=>'Morena']);
        \App\District::create(['state_id'=>14,'name'=>'Narsinghpur']);
        \App\District::create(['state_id'=>14,'name'=>'Neemuch']);
        \App\District::create(['state_id'=>14,'name'=>'Panna']);
        \App\District::create(['state_id'=>14,'name'=>'Raisen']);
        \App\District::create(['state_id'=>14,'name'=>'Rajgarh']);
        \App\District::create(['state_id'=>14,'name'=>'Ratlam']);
        \App\District::create(['state_id'=>14,'name'=>'Rewa']);
        \App\District::create(['state_id'=>14,'name'=>'Sagar']);
        \App\District::create(['state_id'=>14,'name'=>'Satna']);
        \App\District::create(['state_id'=>14,'name'=>'Sehore']);
        \App\District::create(['state_id'=>14,'name'=>'Seoni']);
        \App\District::create(['state_id'=>14,'name'=>'Shahdol']);
        \App\District::create(['state_id'=>14,'name'=>'Shajapur']);
        \App\District::create(['state_id'=>14,'name'=>'Sheopur']);
        \App\District::create(['state_id'=>14,'name'=>'Shivpuri']);
        \App\District::create(['state_id'=>14,'name'=>'Sidhi']);
        \App\District::create(['state_id'=>14,'name'=>'Singrauli']);
        \App\District::create(['state_id'=>14,'name'=>'Tikamgarh']);
        \App\District::create(['state_id'=>14,'name'=>'Ujjain']);
        \App\District::create(['state_id'=>14,'name'=>'Umaria']);
        \App\District::create(['state_id'=>14,'name'=>'Vidisha']);

        //Maharashtra
        \App\District::create(['state_id'=>15,'name'=>'Ahmednagar']);
        \App\District::create(['state_id'=>15,'name'=>'Akola']);
        \App\District::create(['state_id'=>15,'name'=>'Amravati']);
        \App\District::create(['state_id'=>15,'name'=>'Aurangabad']);
        \App\District::create(['state_id'=>15,'name'=>'Beed']);
        \App\District::create(['state_id'=>15,'name'=>'Bhandara']);
        \App\District::create(['state_id'=>15,'name'=>'Buldhana']);
        \App\District::create(['state_id'=>15,'name'=>'Chandrapur']);
        \App\District::create(['state_id'=>15,'name'=>'Dhule']);
        \App\District::create(['state_id'=>15,'name'=>'Gadchiroli']);
        \App\District::create(['state_id'=>15,'name'=>'Gondia']);
        \App\District::create(['state_id'=>15,'name'=>'Hingoli']);
        \App\District::create(['state_id'=>15,'name'=>'Jalgaon']);
        \App\District::create(['state_id'=>15,'name'=>'Jalna']);
        \App\District::create(['state_id'=>15,'name'=>'Kolhapur']);
        \App\District::create(['state_id'=>15,'name'=>'Latur']);
        \App\District::create(['state_id'=>15,'name'=>'Mumbai City']);
        \App\District::create(['state_id'=>15,'name'=>'Mumbai suburban']);
        \App\District::create(['state_id'=>15,'name'=>'Nanded']);
        \App\District::create(['state_id'=>15,'name'=>'Nandurbar']);
        \App\District::create(['state_id'=>15,'name'=>'Nagpur']);
        \App\District::create(['state_id'=>15,'name'=>'Nashik']);
        \App\District::create(['state_id'=>15,'name'=>'Osmanabad']);
        \App\District::create(['state_id'=>15,'name'=>'Palghar']);
        \App\District::create(['state_id'=>15,'name'=>'Parbhani']);
        \App\District::create(['state_id'=>15,'name'=>'Pune']);
        \App\District::create(['state_id'=>15,'name'=>'Raigad']);
        \App\District::create(['state_id'=>15,'name'=>'Ratnagiri']);
        \App\District::create(['state_id'=>15,'name'=>'Sangli']);
        \App\District::create(['state_id'=>15,'name'=>'Satara']);
        \App\District::create(['state_id'=>15,'name'=>'Sindhudurg']);
        \App\District::create(['state_id'=>15,'name'=>'Solapur']);
        \App\District::create(['state_id'=>15,'name'=>'Thane']);
        \App\District::create(['state_id'=>15,'name'=>'Wardha']);
        \App\District::create(['state_id'=>15,'name'=>'Washim']);
        \App\District::create(['state_id'=>15,'name'=>'Yavatmal']);

        //Manipur
        \App\District::create(['state_id'=>16,'name'=>'Bishnupur']);
        \App\District::create(['state_id'=>16,'name'=>'Churachandpur']);
        \App\District::create(['state_id'=>16,'name'=>'Chandel']);
        \App\District::create(['state_id'=>16,'name'=>'Imphal East']);
        \App\District::create(['state_id'=>16,'name'=>'Senapati']);
        \App\District::create(['state_id'=>16,'name'=>'Tamenglong']);
        \App\District::create(['state_id'=>16,'name'=>'Thoubal']);
        \App\District::create(['state_id'=>16,'name'=>'Ukhrul']);
        \App\District::create(['state_id'=>16,'name'=>'Imphal West']);

        //Meghalaya
        \App\District::create(['state_id'=>17,'name'=>'East Garo Hills']);
        \App\District::create(['state_id'=>17,'name'=>'East Khasi Hills']);
        \App\District::create(['state_id'=>17,'name'=>'East Jaintia Hills']);
        \App\District::create(['state_id'=>17,'name'=>'North Garo Hills']);
        \App\District::create(['state_id'=>17,'name'=>'Ri Bhoi']);
        \App\District::create(['state_id'=>17,'name'=>'South Garo Hills']);
        \App\District::create(['state_id'=>17,'name'=>'South West Garo Hills']);
        \App\District::create(['state_id'=>17,'name'=>'South West Khasi Hills']);
        \App\District::create(['state_id'=>17,'name'=>'West Jaintia Hills']);
        \App\District::create(['state_id'=>17,'name'=>'West Garo Hills']);
        \App\District::create(['state_id'=>17,'name'=>'West Khasi Hills']);

        //Mizoram
        \App\District::create(['state_id'=>18,'name'=>'Aizawl']);
        \App\District::create(['state_id'=>18,'name'=>'Champhai']);
        \App\District::create(['state_id'=>18,'name'=>'Kolasib']);
        \App\District::create(['state_id'=>18,'name'=>'Lawngtlai']);
        \App\District::create(['state_id'=>18,'name'=>'Lunglei']);
        \App\District::create(['state_id'=>18,'name'=>'Mamit']);
        \App\District::create(['state_id'=>18,'name'=>'Saiha']);
        \App\District::create(['state_id'=>18,'name'=>'Serchhip']);

        //Nagaland
        \App\District::create(['state_id'=>19,'name'=>'Dimapur']);
        \App\District::create(['state_id'=>19,'name'=>'Kiphire']);
        \App\District::create(['state_id'=>19,'name'=>'Kohima']);
        \App\District::create(['state_id'=>19,'name'=>'Longleng']);
        \App\District::create(['state_id'=>19,'name'=>'Mokokchung']);
        \App\District::create(['state_id'=>19,'name'=>'Mon']);
        \App\District::create(['state_id'=>19,'name'=>'Peren']);
        \App\District::create(['state_id'=>19,'name'=>'Phek']);
        \App\District::create(['state_id'=>19,'name'=>'Tuensang']);
        \App\District::create(['state_id'=>19,'name'=>'Wokha']);
        \App\District::create(['state_id'=>19,'name'=>'Zunheboto']);

        //Odisha
        \App\District::create(['state_id'=>20,'name'=>'Angul']);
        \App\District::create(['state_id'=>20,'name'=>'Boudh (Bauda)']);
        \App\District::create(['state_id'=>20,'name'=>'Bhadrak']);
        \App\District::create(['state_id'=>20,'name'=>'Balangir']);
        \App\District::create(['state_id'=>20,'name'=>'Bargarh (Baragarh)']);
        \App\District::create(['state_id'=>20,'name'=>'Balasore']);
        \App\District::create(['state_id'=>20,'name'=>'Cuttack']);
        \App\District::create(['state_id'=>20,'name'=>'Debagarh (Deogarh)']);
        \App\District::create(['state_id'=>20,'name'=>'Dhenkanal']);
        \App\District::create(['state_id'=>20,'name'=>'Ganjam']);
        \App\District::create(['state_id'=>20,'name'=>'Gajapati']);
        \App\District::create(['state_id'=>20,'name'=>'Jharsuguda']);
        \App\District::create(['state_id'=>20,'name'=>'Jajpur']);
        \App\District::create(['state_id'=>20,'name'=>'Jagatsinghpur']);
        \App\District::create(['state_id'=>20,'name'=>'Khordha']);
        \App\District::create(['state_id'=>20,'name'=>'Kendujhar (Keonjhar)']);
        \App\District::create(['state_id'=>20,'name'=>'Kalahandi']);
        \App\District::create(['state_id'=>20,'name'=>'Kandhamal']);
        \App\District::create(['state_id'=>20,'name'=>'Koraput']);
        \App\District::create(['state_id'=>20,'name'=>'Kendrapara']);
        \App\District::create(['state_id'=>20,'name'=>'Malkangiri']);
        \App\District::create(['state_id'=>20,'name'=>'Mayurbhanj']);
        \App\District::create(['state_id'=>20,'name'=>'Nabarangpur']);
        \App\District::create(['state_id'=>20,'name'=>'Nuapada']);
        \App\District::create(['state_id'=>20,'name'=>'Nayagarh']);
        \App\District::create(['state_id'=>20,'name'=>'Puri']);
        \App\District::create(['state_id'=>20,'name'=>'Rayagada']);
        \App\District::create(['state_id'=>20,'name'=>'Sambalpur']);
        \App\District::create(['state_id'=>20,'name'=>'Subarnapur (Sonepur)']);
        \App\District::create(['state_id'=>20,'name'=>'Sundargarh']);

        //Puducherry
        \App\District::create(['state_id'=>36,'name'=>'Karaikal']);
        \App\District::create(['state_id'=>36,'name'=>'Mahe']);
        \App\District::create(['state_id'=>36,'name'=>'Pondicherry']);
        \App\District::create(['state_id'=>36,'name'=>'Yanam']);

        //Punjab
        \App\District::create(['state_id'=>21,'name'=>'Amritsar']);
        \App\District::create(['state_id'=>21,'name'=>'Barnala']);
        \App\District::create(['state_id'=>21,'name'=>'Bathinda']);
        \App\District::create(['state_id'=>21,'name'=>'Firozpur']);
        \App\District::create(['state_id'=>21,'name'=>'Faridkot']);
        \App\District::create(['state_id'=>21,'name'=>'Fatehgarh Sahib']);
        \App\District::create(['state_id'=>21,'name'=>'Fazilka']);
        \App\District::create(['state_id'=>21,'name'=>'Gurdaspur']);
        \App\District::create(['state_id'=>21,'name'=>'Hoshiarpur']);
        \App\District::create(['state_id'=>21,'name'=>'Jalandhar']);
        \App\District::create(['state_id'=>21,'name'=>'Kapurthala']);
        \App\District::create(['state_id'=>21,'name'=>'Ludhiana']);
        \App\District::create(['state_id'=>21,'name'=>'Mansa']);
        \App\District::create(['state_id'=>21,'name'=>'Moga']);
        \App\District::create(['state_id'=>21,'name'=>'Sri Muktsar Sahib']);
        \App\District::create(['state_id'=>21,'name'=>'Pathankot']);
        \App\District::create(['state_id'=>21,'name'=>'Patiala']);
        \App\District::create(['state_id'=>21,'name'=>'Rupnagar']);
        \App\District::create(['state_id'=>21,'name'=>'Sahibzada Ajit Singh Nagar']);
        \App\District::create(['state_id'=>21,'name'=>'Sangrur']);
        \App\District::create(['state_id'=>21,'name'=>'Shahid Bhagat Singh Nagar']);
        \App\District::create(['state_id'=>21,'name'=>'Tarn Taran']);

        //Rajasthan
        \App\District::create(['state_id'=>22,'name'=>'Ajmer']);
        \App\District::create(['state_id'=>22,'name'=>'Alwar']);
        \App\District::create(['state_id'=>22,'name'=>'Bikaner']);
        \App\District::create(['state_id'=>22,'name'=>'Barmer']);
        \App\District::create(['state_id'=>22,'name'=>'Banswara']);
        \App\District::create(['state_id'=>22,'name'=>'Bharatpur']);
        \App\District::create(['state_id'=>22,'name'=>'Baran']);
        \App\District::create(['state_id'=>22,'name'=>'Bundi']);
        \App\District::create(['state_id'=>22,'name'=>'Bhilwara']);
        \App\District::create(['state_id'=>22,'name'=>'Churu']);
        \App\District::create(['state_id'=>22,'name'=>'Chittorgarh']);
        \App\District::create(['state_id'=>22,'name'=>'Dausa']);
        \App\District::create(['state_id'=>22,'name'=>'Dholpur']);
        \App\District::create(['state_id'=>22,'name'=>'Dungarpur']);
        \App\District::create(['state_id'=>22,'name'=>'Ganganagar']);
        \App\District::create(['state_id'=>22,'name'=>'Hanumangarh']);
        \App\District::create(['state_id'=>22,'name'=>'Jhunjhunu']);
        \App\District::create(['state_id'=>22,'name'=>'Jalore']);
        \App\District::create(['state_id'=>22,'name'=>'Jodhpur']);
        \App\District::create(['state_id'=>22,'name'=>'Jaipur']);
        \App\District::create(['state_id'=>22,'name'=>'Jaisalmer']);
        \App\District::create(['state_id'=>22,'name'=>'Jhalawar']);
        \App\District::create(['state_id'=>22,'name'=>'Karauli']);
        \App\District::create(['state_id'=>22,'name'=>'Kota']);
        \App\District::create(['state_id'=>22,'name'=>'Nagaur']);
        \App\District::create(['state_id'=>22,'name'=>'Pali']);
        \App\District::create(['state_id'=>22,'name'=>'Pratapgarh']);
        \App\District::create(['state_id'=>22,'name'=>'Rajsamand']);
        \App\District::create(['state_id'=>22,'name'=>'Sikar']);
        \App\District::create(['state_id'=>22,'name'=>'Sawai Madhopur']);
        \App\District::create(['state_id'=>22,'name'=>'Sirohi']);
        \App\District::create(['state_id'=>22,'name'=>'Tonk']);
        \App\District::create(['state_id'=>22,'name'=>'Udaipur']);

        //Sikkim
        \App\District::create(['state_id'=>23,'name'=>'East Sikkim']);
        \App\District::create(['state_id'=>23,'name'=>'North Sikkim']);
        \App\District::create(['state_id'=>23,'name'=>'South Sikkim']);
        \App\District::create(['state_id'=>23,'name'=>'West Sikkim']);

        //Tamil Nadu
        \App\District::create(['state_id'=>24,'name'=>'Ariyalur']);
        \App\District::create(['state_id'=>24,'name'=>'Chennai']);
        \App\District::create(['state_id'=>24,'name'=>'Coimbatore']);
        \App\District::create(['state_id'=>24,'name'=>'Cuddalore']);
        \App\District::create(['state_id'=>24,'name'=>'Dharmapuri']);
        \App\District::create(['state_id'=>24,'name'=>'Dindigul']);
        \App\District::create(['state_id'=>24,'name'=>'Erode']);
        \App\District::create(['state_id'=>24,'name'=>'Kanchipuram']);
        \App\District::create(['state_id'=>24,'name'=>'Kanyakumari']);
        \App\District::create(['state_id'=>24,'name'=>'Karur']);
        \App\District::create(['state_id'=>24,'name'=>'Krishnagiri']);
        \App\District::create(['state_id'=>24,'name'=>'Madurai']);
        \App\District::create(['state_id'=>24,'name'=>'Nagapattinam']);
        \App\District::create(['state_id'=>24,'name'=>'Nilgiris']);
        \App\District::create(['state_id'=>24,'name'=>'Namakkal']);
        \App\District::create(['state_id'=>24,'name'=>'Perambalur']);
        \App\District::create(['state_id'=>24,'name'=>'Pudukkottai']);
        \App\District::create(['state_id'=>24,'name'=>'Ramanathapuram']);
        \App\District::create(['state_id'=>24,'name'=>'Salem']);
        \App\District::create(['state_id'=>24,'name'=>'Sivaganga']);
        \App\District::create(['state_id'=>24,'name'=>'Tirupur']);
        \App\District::create(['state_id'=>24,'name'=>'Tiruchirappalli']);
        \App\District::create(['state_id'=>24,'name'=>'Theni']);
        \App\District::create(['state_id'=>24,'name'=>'Tirunelveli']);
        \App\District::create(['state_id'=>24,'name'=>'Thanjavur']);
        \App\District::create(['state_id'=>24,'name'=>'Thoothukudi']);
        \App\District::create(['state_id'=>24,'name'=>'Tiruvallur']);
        \App\District::create(['state_id'=>24,'name'=>'Tiruvarur']);
        \App\District::create(['state_id'=>24,'name'=>'Tiruvannamalai']);
        \App\District::create(['state_id'=>24,'name'=>'Vellore']);
        \App\District::create(['state_id'=>24,'name'=>'Viluppuram']);
        \App\District::create(['state_id'=>24,'name'=>'Virudhunagar']);

        //Telangana
        \App\District::create(['state_id'=>29,'name'=>'Adilabad']);
        \App\District::create(['state_id'=>29,'name'=>'Komaram Bheem Asifabad']);
        \App\District::create(['state_id'=>29,'name'=>'Bhadradri Kothagudem']);
        \App\District::create(['state_id'=>29,'name'=>'Hyderabad']);
        \App\District::create(['state_id'=>29,'name'=>'Jagtial']);
        \App\District::create(['state_id'=>29,'name'=>'Jangaon']);
        \App\District::create(['state_id'=>29,'name'=>'Jayashankar Bhupalpally']);
        \App\District::create(['state_id'=>29,'name'=>'Jogulamba Gadwal']);
        \App\District::create(['state_id'=>29,'name'=>'Kamareddy']);
        \App\District::create(['state_id'=>29,'name'=>'Karimnagar']);
        \App\District::create(['state_id'=>29,'name'=>'Khammam']);
        \App\District::create(['state_id'=>29,'name'=>'Mahabubabad']);
        \App\District::create(['state_id'=>29,'name'=>'Mahbubnagar']);
        \App\District::create(['state_id'=>29,'name'=>'Mancherial']);
        \App\District::create(['state_id'=>29,'name'=>'Medak']);
        \App\District::create(['state_id'=>29,'name'=>'Medchal']);
        \App\District::create(['state_id'=>29,'name'=>'Nalgonda']);
        \App\District::create(['state_id'=>29,'name'=>'Nagarkurnool']);
        \App\District::create(['state_id'=>29,'name'=>'Nirmal']);
        \App\District::create(['state_id'=>29,'name'=>'Nizamabad']);
        \App\District::create(['state_id'=>29,'name'=>'Peddapalli']);
        \App\District::create(['state_id'=>29,'name'=>'Rajanna Sircilla']);
        \App\District::create(['state_id'=>29,'name'=>'Ranga Reddy']);
        \App\District::create(['state_id'=>29,'name'=>'Sangareddy']);
        \App\District::create(['state_id'=>29,'name'=>'Siddipet']);
        \App\District::create(['state_id'=>29,'name'=>'Suryapet']);
        \App\District::create(['state_id'=>29,'name'=>'Vikarabad']);
        \App\District::create(['state_id'=>29,'name'=>'Wanaparthy']);
        \App\District::create(['state_id'=>29,'name'=>'Warangal (urban)']);
        \App\District::create(['state_id'=>29,'name'=>'Warangal (rural)']);
        \App\District::create(['state_id'=>29,'name'=>'Yadadri Bhuvanagiri']);

        //Tripura
        \App\District::create(['state_id'=>25,'name'=>'Dhalai']);
        \App\District::create(['state_id'=>25,'name'=>'Gomati']);
        \App\District::create(['state_id'=>25,'name'=>'Khowai']);
        \App\District::create(['state_id'=>25,'name'=>'North Tripura']);
        \App\District::create(['state_id'=>25,'name'=>'Sepahijala']);
        \App\District::create(['state_id'=>25,'name'=>'South Tripura']);
        \App\District::create(['state_id'=>25,'name'=>'Unokoti']);
        \App\District::create(['state_id'=>25,'name'=>'West Tripura']);

        //Uttar Pradesh
        \App\District::create(['state_id'=>26,'name'=>'Agra']);
        \App\District::create(['state_id'=>26,'name'=>'Aligarh']);
        \App\District::create(['state_id'=>26,'name'=>'Allahabad']);
        \App\District::create(['state_id'=>26,'name'=>'Ambedkar Nagar']);
        \App\District::create(['state_id'=>26,'name'=>'Amethi (Chhatrapati Shahuji Maharaj Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Amroha (Jyotiba Phule Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Auraiya']);
        \App\District::create(['state_id'=>26,'name'=>'Azamgarh']);
        \App\District::create(['state_id'=>26,'name'=>'Bagpat']);
        \App\District::create(['state_id'=>26,'name'=>'Bahraich']);
        \App\District::create(['state_id'=>26,'name'=>'Ballia']);
        \App\District::create(['state_id'=>26,'name'=>'Balrampur']);
        \App\District::create(['state_id'=>26,'name'=>'Banda']);
        \App\District::create(['state_id'=>26,'name'=>'Barabanki']);
        \App\District::create(['state_id'=>26,'name'=>'Basti']);
        \App\District::create(['state_id'=>26,'name'=>'Bijnor']);
        \App\District::create(['state_id'=>26,'name'=>'Budaun']);
        \App\District::create(['state_id'=>26,'name'=>'Bulandshahr']);
        \App\District::create(['state_id'=>26,'name'=>'Chandauli']);
        \App\District::create(['state_id'=>26,'name'=>'Chitrakoot']);
        \App\District::create(['state_id'=>26,'name'=>'Deoria']);
        \App\District::create(['state_id'=>26,'name'=>'Etah']);
        \App\District::create(['state_id'=>26,'name'=>'Etawah']);
        \App\District::create(['state_id'=>26,'name'=>'Faizabad']);
        \App\District::create(['state_id'=>26,'name'=>'Farrukhabad']);
        \App\District::create(['state_id'=>26,'name'=>'Fatehpur']);
        \App\District::create(['state_id'=>26,'name'=>'Firozabad']);
        \App\District::create(['state_id'=>26,'name'=>'Gautam Buddh Nagar']);
        \App\District::create(['state_id'=>26,'name'=>'Ghaziabad']);
        \App\District::create(['state_id'=>26,'name'=>'Ghazipur']);
        \App\District::create(['state_id'=>26,'name'=>'Gonda']);
        \App\District::create(['state_id'=>26,'name'=>'Gorakhpur']);
        \App\District::create(['state_id'=>26,'name'=>'Hamirpur']);
        \App\District::create(['state_id'=>26,'name'=>'Hapur (Panchsheel Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Hardoi']);
        \App\District::create(['state_id'=>26,'name'=>'Hathras (Mahamaya Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Jalaun']);
        \App\District::create(['state_id'=>26,'name'=>'Jaunpur']);
        \App\District::create(['state_id'=>26,'name'=>'Jhansi']);
        \App\District::create(['state_id'=>26,'name'=>'Kannauj']);
        \App\District::create(['state_id'=>26,'name'=>'Kanpur Dehat (Ramabai Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Kanpur Nagar']);
        \App\District::create(['state_id'=>26,'name'=>'Kasganj (Kanshi Ram Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Kaushambi']);
        \App\District::create(['state_id'=>26,'name'=>'Kushinagar']);
        \App\District::create(['state_id'=>26,'name'=>'Lakhimpur Kheri']);
        \App\District::create(['state_id'=>26,'name'=>'Lalitpur']);
        \App\District::create(['state_id'=>26,'name'=>'Lucknow']);
        \App\District::create(['state_id'=>26,'name'=>'Maharajganj']);
        \App\District::create(['state_id'=>26,'name'=>'Mahoba']);
        \App\District::create(['state_id'=>26,'name'=>'Mainpuri']);
        \App\District::create(['state_id'=>26,'name'=>'Mathura']);
        \App\District::create(['state_id'=>26,'name'=>'Mau']);
        \App\District::create(['state_id'=>26,'name'=>'Meerut']);
        \App\District::create(['state_id'=>26,'name'=>'Mirzapur']);
        \App\District::create(['state_id'=>26,'name'=>'Moradabad']);
        \App\District::create(['state_id'=>26,'name'=>'Muzaffarnagar']);
        \App\District::create(['state_id'=>26,'name'=>'Pilibhit']);
        \App\District::create(['state_id'=>26,'name'=>'Pratapgarh']);
        \App\District::create(['state_id'=>26,'name'=>'Raebareli']);
        \App\District::create(['state_id'=>26,'name'=>'Rampur']);
        \App\District::create(['state_id'=>26,'name'=>'Saharanpur']);
        \App\District::create(['state_id'=>26,'name'=>'Sambhal (Bheem Nagar)']);
        \App\District::create(['state_id'=>26,'name'=>'Sant Kabir Nagar']);
        \App\District::create(['state_id'=>26,'name'=>'Sant Ravidas Nagar']);
        \App\District::create(['state_id'=>26,'name'=>'Shahjahanpur']);
        \App\District::create(['state_id'=>26,'name'=>'Shamli']);
        \App\District::create(['state_id'=>26,'name'=>'Shravasti']);
        \App\District::create(['state_id'=>26,'name'=>'Siddharthnagar']);
        \App\District::create(['state_id'=>26,'name'=>'Sitapur']);
        \App\District::create(['state_id'=>26,'name'=>'Sonbhadra']);
        \App\District::create(['state_id'=>26,'name'=>'Sultanpur']);
        \App\District::create(['state_id'=>26,'name'=>'Unnao']);
        \App\District::create(['state_id'=>26,'name'=>'Varanasi']);

        //Uttarakhand
        \App\District::create(['state_id'=>27,'name'=>'Almora']);
        \App\District::create(['state_id'=>27,'name'=>'Bageshwar']);
        \App\District::create(['state_id'=>27,'name'=>'Chamoli']);
        \App\District::create(['state_id'=>27,'name'=>'Champawat']);
        \App\District::create(['state_id'=>27,'name'=>'Dehradun']);
        \App\District::create(['state_id'=>27,'name'=>'Haridwar']);
        \App\District::create(['state_id'=>27,'name'=>'Nainital']);
        \App\District::create(['state_id'=>27,'name'=>'Pauri Garhwal']);
        \App\District::create(['state_id'=>27,'name'=>'Pithoragarh']);
        \App\District::create(['state_id'=>27,'name'=>'Rudraprayag']);
        \App\District::create(['state_id'=>27,'name'=>'Tehri Garhwal']);
        \App\District::create(['state_id'=>27,'name'=>'Udham Singh Nagar']);
        \App\District::create(['state_id'=>27,'name'=>'Uttarkashi']);

        //West Bengal
        \App\District::create(['state_id'=>28,'name'=>'Alipurduar']);
        \App\District::create(['state_id'=>28,'name'=>'Bankura']);
        \App\District::create(['state_id'=>28,'name'=>'Bardhaman']);
        \App\District::create(['state_id'=>28,'name'=>'Birbhum']);
        \App\District::create(['state_id'=>28,'name'=>'Cooch Behar']);
        \App\District::create(['state_id'=>28,'name'=>'Dakshin Dinajpur']);
        \App\District::create(['state_id'=>28,'name'=>'Darjeeling']);
        \App\District::create(['state_id'=>28,'name'=>'Hooghly']);
        \App\District::create(['state_id'=>28,'name'=>'Howrah']);
        \App\District::create(['state_id'=>28,'name'=>'Jalpaiguri']);
        \App\District::create(['state_id'=>28,'name'=>'Jhargram']);
        \App\District::create(['state_id'=>28,'name'=>'Kalimpong']);
        \App\District::create(['state_id'=>28,'name'=>'Kolkata']);
        \App\District::create(['state_id'=>28,'name'=>'Maldah']);
        \App\District::create(['state_id'=>28,'name'=>'Murshidabad']);
        \App\District::create(['state_id'=>28,'name'=>'Nadia']);
        \App\District::create(['state_id'=>28,'name'=>'North 24 Parganas']);
        \App\District::create(['state_id'=>28,'name'=>'Paschim Medinipur']);
        \App\District::create(['state_id'=>28,'name'=>'Purba Medinipur']);
        \App\District::create(['state_id'=>28,'name'=>'Purulia']);
        \App\District::create(['state_id'=>28,'name'=>'South 24 Parganas']);
        \App\District::create(['state_id'=>28,'name'=>'Uttar Dinajpur']);
        //DISTRICT SEEDING END

        $info->info('Client table seeding started...');
        $this->call('ClientSeeder');

        $info->info('Feature_list table seeding started...');
        $this->call('Feature_listSeeder');

        $info->info('Latest_deal table seeding started...');
        $this->call('Latest_dealSeeder');

        $info->info('Photo table seeding started...');
        $this->call('PhotoSeeder');

        $info->info('Property_feature table seeding started...');
        $this->call('Property_featureSeeder');

        $info->info('Property_type table seeding started...');
        $this->call('Property_typeSeeder');

        $info->info('Testimonial table seeding started...');
        $this->call('TestimonialSeeder');


        $info->info('Property table seeding started...');
        $this->call('PropertySeeder');

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Blog table seeding started...');
        $this->call('BlogSeeder');

        $info->info('Contact table seeding started...');
        $this->call('ContactSeeder');


        $info->info('FeaturedProperty table seeding started...');
        $this->call('Featured_PropertySeeder');

        $info->info('GetQuote table seeding started...');
        $this->call('GetQuoteSeeder');

        $info->info('Gallery table seeding started...');
        $this->call('GallerySeeder');

        $info->info('Moment table seeding started...');
        $this->call('MomentSeeder');

        $info->error('Seeding Completed.........');

        Model::reguard();
    }
}
