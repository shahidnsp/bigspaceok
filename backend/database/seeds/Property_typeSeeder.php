<?php

use Illuminate\Database\Seeder;

class Property_typeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $property_type=factory(App\PropertyType::class,10)->create();

    }
}
