app.controller('EnquiryController', function($scope,$http,$anchorScroll,ngNotify,Enquiry,$modal){
    $scope.enquiries=[];
    $scope.enquiryedit=false;
    
    

    Enquiry.query(function(enquiry){
        $scope.enquiries=enquiry;
    });
    


    $scope.newEnquiry = function (argument) {
        $scope.enquiryedit = true;
        $scope.newenquiry = new Enquiry();
        $scope.curEnquiry = {};
    };
    $scope.editEnquiry = function (thisEnquiry) {
        $scope.enquiryedit = true;
        $scope.curEnquiry =  thisEnquiry;
        $scope.newenquiry = angular.copy(thisEnquiry);
        $anchorScroll();
    };

    $scope.cancelEnquiry = function () {
        $scope.enquiryedit = false;
        $scope.newenquiry = new Enquiry();
    };
    
    
    $scope.addEnquiry = function () {

        if ($scope.curEnquiry.id) {
            $scope.newenquiry.$update(function(enquiry){
                angular.extend($scope.curEnquiry, $scope.curEnquiry, enquiry);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Updated Successfully');
            });
        } else{
            $scope.newenquiry.$save(function(enquiry){
                $scope.enquiries.push(enquiry);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Saved Successfully');
            });
        }
        $scope.enquiryedit = false;
        $scope.newenquiry = new Enquiry();
    };


    $scope.deleteEnquiry = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.enquiries.indexOf(item);
                $scope.enquiries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Removed Successfully');
            });
        }
    };

    $scope.viewEnquiry = function (p) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewenquiry',
            controller:'ViewEnquiryController',
            size: 'lg',
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    }; 
    
});



app.controller('ViewEnquiryController', function ($scope, $modalInstance,$modal, item) {

    $scope.enquiry = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

});