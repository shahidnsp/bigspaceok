app.controller('ClientController', function($scope,$http,$anchorScroll,ngNotify,Client){
    $scope.clients=[];
    $scope.clientedit=false;

    Client.query(function(client){
        $scope.clients=client;
    });

    $scope.newClient = function (argument) {
        $scope.clientedit = true;
        $scope.newclient = new Client();
        $scope.curClient = {};
    };
    $scope.editClient = function (thisClient) {
        $scope.clientedit = true;
        $scope.curClient =  thisClient;
        $scope.newclient = angular.copy(thisClient);
        $anchorScroll();
    };
    $scope.addClient = function () {

        if ($scope.curClient.id) {
            $scope.newclient.$update(function(client){
                angular.extend($scope.curClient, $scope.curClient, client);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Updated Successfully');
            });
        } else{
            $scope.newclient.$save(function(client){
                $scope.clients.push(client);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Saved Successfully');
            });
        }
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
    $scope.deleteClient = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.clients.indexOf(item);
                $scope.clients.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Removed Successfully');
            });
        }
    };
    $scope.cancelClient = function () {
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
});