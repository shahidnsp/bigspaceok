app.controller('PropertyController', function($scope,$http,$anchorScroll,$modal,ngNotify,Property){
    $scope.propertys=[];
    $scope.propertyedit=false;


    $scope.districts=[];
    $scope.getDistrict=function(state_id){
        $http.get('/getDistricts',{params:{state_id:state_id}}).
            success(function(data, status)
            {
                $scope.districts=data;
            });
    };

    $http.get('/getState').
        success(function(data, status)
        {
            $scope.states=data;
        });


    Property.query(function(property){
        $scope.propertys=property;
    });

    $scope.newProperty = function (argument) {
        $scope.propertyedit = true;
        $scope.newproperty = new Property();
        $scope.curProperty = {};
    };
    $scope.editProperty = function (thisProperty) {
        $scope.propertyedit = true;
        $scope.curProperty =  thisProperty;
        $scope.newproperty = angular.copy(thisProperty);
        $anchorScroll();
    };

    $scope.addProperty = function () {

        if ($scope.curProperty.id) {
            $scope.newproperty.$update(function(property){
                angular.extend($scope.curProperty, $scope.curProperty, property);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Updated Successfully');
            });
        } else{
            $scope.newproperty.$save(function(property){
                $scope.propertys.push(property);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Saved Successfully');
            });
        }
        $scope.propertyedit = false;
        $scope.newproperty = new Property();
    };
    $scope.deleteProperty = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertys.indexOf(item);
                $scope.propertys.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Removed Successfully');
            });
        }
    };
    $scope.cancelProperty = function () {
        $scope.propertyedit = false;
        $scope.newproperty = new Property();
    };


    $scope.viewProperty = function (p) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproperty',
            controller:'ViewPropertyController',
            size: 'lg',
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

    $scope.deletePhoto=function(item){
        $http.delete('api/photo/'+item.id).
            success(function (data, status, headers, config) {
                var curIndex = $scope.newproperty.photos.indexOf(item);
                $scope.newproperty.photos.splice(curIndex, 1);
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

});

app.controller('ViewPropertyController', function ($scope, $modalInstance,$modal, item) {

    $scope.property = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

});
