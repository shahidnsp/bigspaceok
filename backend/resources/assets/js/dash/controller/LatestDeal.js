app.controller('LatestDealController', function($scope,$http,$anchorScroll,ngNotify,LatestDeal){
    $scope.latestDeals=[];
    $scope.latestDealedit=false;

    LatestDeal.query(function(latestDeal){
        $scope.latestDeals=latestDeal;
    });

    $scope.newLatestDeal = function (argument) {
        $scope.latestDealedit = true;
        $scope.newlatestDeal = new LatestDeal();
        $scope.curLatestDeal = {};
    };
    $scope.editLatestDeal = function (thisLatestDeal) {
        $scope.latestDealedit = true;
        $scope.curLatestDeal =  thisLatestDeal;
        $scope.newlatestDeal = angular.copy(thisLatestDeal);
        $anchorScroll();
    };
    $scope.addLatestDeal = function () {

        if ($scope.curLatestDeal.id) {
            $scope.newlatestDeal.$update(function(latestDeal){
                angular.extend($scope.curLatestDeal, $scope.curLatestDeal, latestDeal);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Updated Successfully');
            });
        } else{
            $scope.newlatestDeal.$save(function(latestDeal){
                $scope.latestDeals.push(latestDeal);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Saved Successfully');
            });
        }
        $scope.latestDealedit = false;
        $scope.newlatestDeal = new LatestDeal();
    };
    $scope.deleteLatestDeal = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.latestDeals.indexOf(item);
                $scope.latestDeals.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Removed Successfully');
            });
        }
    };
    $scope.cancelLatestDeal = function () {
        $scope.latestDealedit = false;
        $scope.newlatestDeal = new LatestDeal();
    };

    $scope.deletePhoto=function(item){
        $http.delete('api/photo/'+item.id).
            success(function (data, status, headers, config) {
                var curIndex = $scope.newlatestDeal.photos.indexOf(item);
                $scope.newlatestDeal.photos.splice(curIndex, 1);
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);

            });
    };
});