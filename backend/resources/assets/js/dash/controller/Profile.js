app.controller('ProfileController', function($scope,$http,$anchorScroll,ngNotify,User,UserInfo){
    
    $scope.users=[];

    $scope.useredit=false;

    User.query(function(user){
        $scope.users=user;
    });

    $scope.newUser = function (argument) {
        $scope.useredit = true;
        $scope.newuser = new User();
        $scope.curUser = {};
        $scope.newuser.isAdmin=0;
    };
    $scope.editUser = function (thisUser) {
        $scope.useredit = true;
        $scope.curUser =  thisUser;
        $scope.newuser = angular.copy(thisUser);
        $anchorScroll();
    };

    $scope.addUser = function () {

        if ($scope.curUser.id) {
            $scope.newuser.$update(function(User){
                angular.extend($scope.curUser, $scope.curUser, User);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Updated Successfully');
            });
        } else{
            $scope.newuser.$save(function(User){
                $scope.users.push(User);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Saved Successfully....Default Password is admin');
            });
        }
        $scope.useredit = false;
        $scope.newuser = new User();
    };
    $scope.deleteUser = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.users.indexOf(item);
                $scope.users.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Removed Successfully');
            });
        }
    };
    $scope.cancelUser = function () {
        $scope.useredit = false;
        $scope.newuser = new User();
    };
    
    $scope.updateProfile=function(){
        $http.post('/api/updateProfile',{name:$scope.name,email:$scope.email}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 4000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Profile Changed Successfully...Please Re login to Change Profile name');
            });
    };
    

    $scope.changePassword=function(curpassword,newpassword,repassword){
        $http.post('/api/changepassword',{curpassword:curpassword,newpassword:newpassword,repassword:repassword}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 4000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Password Changed Successfully...Please Re login');
            });
    };


    $scope.resetPassword = function (user,ev) {

        var confirmDelete = confirm("Do you really need to Change Permission ?");
        if (confirmDelete) {
            $http.post('/api/resetPassword',{id:user.id}).
                success(function(data,status,headers,config){
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 4000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set(user.name+'\'s Password Reset to admin');
                }).error(function(data,status,headers,config){
                    console.log(data);

                });
        }

    };


    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
    };

    $scope.savePermission = function (ev){

        var confirmDelete = confirm("Do you really need to Change Permission ?");
        if (confirmDelete) {
            UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                .success(function(data){
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 4000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Permission Changed Successfully..');
                    $scope.resetPermission=false;
                });
        }

    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };
});