app.controller('PropertyTypeController', function($scope,$http,$rootScope,$timeout,$anchorScroll,ngNotify,PropertyType){

    $scope.loading = true;
    $rootScope.$on('$routeChangeSuccess', function () {
        console.log('$routeChangeSuccess');
        //hide loading gif
        $timeout(function(){
            $scope.loading = false;
        }, 2000);
    });


    $scope.propertyTypes=[];
    $scope.propertyTypeedit=false;

    PropertyType.query(function(propertyType){
        $scope.propertyTypes=propertyType;
    });

    $scope.newPropertyType = function (argument) {
        $scope.propertyTypeedit = true;
        $scope.newpropertyType = new PropertyType();
        $scope.curPropertyType = {};
    };
    $scope.editPropertyType = function (thisPropertyType) {
        $scope.propertyTypeedit = true;
        $scope.curPropertyType =  thisPropertyType;
        $scope.newpropertyType = angular.copy(thisPropertyType);
        $anchorScroll();
    };
    $scope.addPropertyType = function () {

        if ($scope.curPropertyType.id) {
            $scope.newpropertyType.$update(function(propertyType){
                angular.extend($scope.curPropertyType, $scope.curPropertyType, propertyType);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Updated Successfully');
            });
        } else{
            $scope.newpropertyType.$save(function(propertyType){
                $scope.propertyTypes.push(propertyType);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Saved Successfully');
            });
        }
        $scope.propertyTypeedit = false;
        $scope.newpropertyType = new PropertyType();
    };
    $scope.deletePropertyType = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertyTypes.indexOf(item);
                $scope.propertyTypes.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Removed Successfully');
            });
        }
    };
    $scope.cancelPropertyType = function () {
        $scope.propertyTypeedit = false;
        $scope.newpropertyType = new PropertyType();
    };
});