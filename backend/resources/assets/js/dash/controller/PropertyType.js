app.controller('PropertyFeatureController', function($scope,$http,$rootScope,$timeout,$anchorScroll,ngNotify,PropertyFeature){

    $scope.loading = true;
    $rootScope.$on('$routeChangeSuccess', function () {
        console.log('$routeChangeSuccess');
        //hide loading gif
        $timeout(function(){
            $scope.loading = false;
        }, 2000);
    });


    $scope.propertyFeatures=[];
    $scope.propertyFeatureedit=false;

    PropertyFeature.query(function(propertyFeature){
       $scope.propertyFeatures=propertyFeature;
    });

    $scope.newPropertyFeature = function (argument) {
        $scope.propertyFeatureedit = true;
        $scope.newpropertyFeature = new PropertyFeature();
        $scope.curPropertyFeature = {};
    };
    $scope.editPropertyFeature = function (thisPropertyFeature) {
        $scope.propertyFeatureedit = true;
        $scope.curPropertyFeature =  thisPropertyFeature;
        $scope.newpropertyFeature = angular.copy(thisPropertyFeature);
        $anchorScroll();
    };
    $scope.addPropertyFeature = function () {

        if ($scope.curPropertyFeature.id) {
            $scope.newpropertyFeature.$update(function(propertyFeature){
                angular.extend($scope.curPropertyFeature, $scope.curPropertyFeature, propertyFeature);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Updated Successfully');
            });
        } else{
            $scope.newpropertyFeature.$save(function(propertyFeature){
                $scope.propertyFeatures.push(propertyFeature);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Saved Successfully');
            });
        }
        $scope.propertyFeatureedit = false;
        $scope.newpropertyFeature = new PropertyFeature();
    };
    $scope.deletePropertyFeature = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertyFeatures.indexOf(item);
                $scope.propertyFeatures.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Removed Successfully');
            });
        }
    };
    $scope.cancelPropertyFeature = function () {
        $scope.propertyFeatureedit = false;
        $scope.newpropertyFeature = new PropertyFeature();
    };
});