app.controller('BlogController', function($scope,$http,$anchorScroll,ngNotify,Blog){
    $scope.blogs=[];
    $scope.blogedit=false;

    Blog.query(function(blog){
        $scope.blogs=blog;
    });

    $scope.newBlog = function (argument) {
        $scope.blogedit = true;
        $scope.newblog = new Blog();
        $scope.curBlog = {};
    };
    $scope.editBlog = function (thisBlog) {
        $scope.blogedit = true;
        $scope.curBlog =  thisBlog;
        $scope.newblog = angular.copy(thisBlog);
        $anchorScroll();
    };
    $scope.addBlog = function () {

        if ($scope.curBlog.id) {
            $scope.newblog.$update(function(blog){
                angular.extend($scope.curBlog, $scope.curBlog, blog);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Updated Successfully');
            });
        } else{
            $scope.newblog.$save(function(blog){
                $scope.blogs.push(blog);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Saved Successfully');
            });
        }
        $scope.blogedit = false;
        $scope.newblog = new Blog();
    };
    $scope.deleteBlog = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.blogs.indexOf(item);
                $scope.blogs.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Removed Successfully');
            });
        }
    };
    $scope.cancelBlog = function () {
        $scope.blogedit = false;
        $scope.newblog = new Blog();
    };
});