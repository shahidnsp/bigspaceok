app.controller('FeaturedPropertyController', function($scope,$http,$anchorScroll,ngNotify,FeaturedProperty,Property){
    $scope.featuredPropertys=[];
    $scope.featuredPropertyedit=false;

    Property.query({isSale:1},function(property){
        $scope.properties=property;
    });

    FeaturedProperty.query(function(featuredProperty){
        $scope.featuredPropertys=featuredProperty;
    });

    $scope.newFeaturedProperty = function (argument) {
        $scope.featuredPropertyedit = true;
        $scope.newfeaturedProperty = new FeaturedProperty();
        $scope.curFeaturedProperty = {};
    };
    $scope.editFeaturedProperty = function (thisFeaturedProperty) {
        $scope.featuredPropertyedit = true;
        $scope.curFeaturedProperty =  thisFeaturedProperty;
        $scope.newfeaturedProperty = angular.copy(thisFeaturedProperty);
        $anchorScroll();
    };
    $scope.addFeaturedProperty = function () {

        if ($scope.curFeaturedProperty.id) {
            $scope.newfeaturedProperty.$update(function(featuredProperty){
                angular.extend($scope.curFeaturedProperty, $scope.curFeaturedProperty, featuredProperty);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Updated Successfully');
            });
        } else{
            $scope.newfeaturedProperty.$save(function(featuredProperty){
                $scope.featuredPropertys.push(featuredProperty);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Saved Successfully');
            });
        }
        $scope.featuredPropertyedit = false;
        $scope.newfeaturedProperty = new FeaturedProperty();
    };
    $scope.deleteFeaturedProperty = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.featuredPropertys.indexOf(item);
                $scope.featuredPropertys.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Removed Successfully');
            });
        }
    };
    $scope.cancelFeaturedProperty = function () {
        $scope.featuredPropertyedit = false;
        $scope.newfeaturedProperty = new FeaturedProperty();
    };
});