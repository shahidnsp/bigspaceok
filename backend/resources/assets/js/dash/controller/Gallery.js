app.controller('GalleryController', function($scope,$http,$window,ngNotify,Gallery){

    $scope.galleries=[];
    Gallery.query(function(gallery){
        $scope.galleries=gallery;
    });

    $scope.galleryedit=false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {

        $scope.newgallery.$save(function(gallery){
            $scope.galleries=[];
            Gallery.query(function(gallery){
                $scope.galleries=gallery;
            });
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('Photos Saved Successfully');
            //$window.location.reload();
        });

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.deletePhoto=function(item){
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.galleries.indexOf(item);
                $scope.galleries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Photo Removed Successfully');

            });
        }
    };
});