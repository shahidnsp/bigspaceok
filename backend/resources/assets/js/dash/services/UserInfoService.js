/**
 * Created by Shahid Neermunda on 11/05/2017.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
    var UserInfo = {};

    UserInfo.query=function(){
        return $http.get('/api/userinfo')
    };


    UserInfo.get = function(id){
        return $http.get('/api/user/'+id);
    };

    UserInfo.save = function(id,data){
        return $http.put('/api/userinfo/'+id,data);
    };

    UserInfo.getAllPage=function(id){
        return $http.get('/api/getAllPages/'+id);
    };

    UserInfo.getUserPermission=function(userId){
        return $http.get('/api/getPermission/?id='+userId);
    };

    UserInfo.setUserPermission = function(data){
        return $http.post('/api/changePermission',data);
    };

    return UserInfo;
}
]);
