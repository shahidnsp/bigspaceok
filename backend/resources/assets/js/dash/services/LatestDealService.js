/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('LatestDealService',[]).factory('LatestDeal',['$resource',
    function($resource){
        return $resource('/api/latestdeal/:latestdealId',{
            latestdealId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);