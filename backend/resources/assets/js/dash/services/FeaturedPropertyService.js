/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('FeaturedPropertyService',[]).factory('FeaturedProperty',['$resource',
    function($resource){
        return $resource('/api/featuredproperty/:featuredpropertyId',{
            featuredpropertyId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);