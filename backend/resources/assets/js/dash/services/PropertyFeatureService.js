/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyFeatureService',[]).factory('PropertyFeature',['$resource',
    function($resource){
        return $resource('/api/propertyfeature/:propertyfeatureId',{
            propertyfeatureId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);