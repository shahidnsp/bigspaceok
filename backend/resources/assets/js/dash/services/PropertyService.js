/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyService',[]).factory('Property',['$resource',
    function($resource){
        return $resource('/api/property/:propertyId',{
            propertyId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);