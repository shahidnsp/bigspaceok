/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('EnquiryService',[]).factory('Enquiry',['$resource',
    function($resource){
        return $resource('/api/quote/:quoteId',{
            quoteId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);