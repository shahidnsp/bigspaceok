/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyTypeService',[]).factory('PropertyType',['$resource',
    function($resource){
        return $resource('/api/propertytype/:propertytypeId',{
            propertytypeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);