/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('BlogService',[]).factory('Blog',['$resource',
    function($resource){
        return $resource('/api/blog/:blogId',{
            blogId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);