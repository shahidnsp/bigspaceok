/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('ClientService',[]).factory('Client',['$resource',
    function($resource){
        return $resource('/api/client/:clientId',{
            clientId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);