<!DOCTYPE html>
<html lang="en" ng-app="myWeb">
<head>
    <title>Big Space OK | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="author" content="Psybo Technologies">
    <!-- External CSS libraries -->
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-submenu.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-select.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/leaflet.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/map.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/flaticon/font/flaticon.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/linearicons/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/jquery.mCustomScrollbar.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/dropzone.css')}}">
   <!-- Custom stylesheet -->
   <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/default.css')}}">
   <!-- Favicon icon -->
   <link rel="shortcut icon" href="{{url('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
   <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <link rel="stylesheet" type="text/css" href="{{url('css/ie10-viewport-bug-workaround.css')}}">
     <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
   <script type="text/javascript" src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
   <script type="text/javascript" src="{{url('js/jquery-2.2.0.min.js')}}"></script>

   <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

   </style>
</head>

<body ng-controller="FormController">
    <div class="page_loader"></div>

    <!-- Top header start -->
    <header class="top-header hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="list-inline">
                        <a href="tel:+919656000111" target="_blank"><i class="fa fa-phone"></i>+91 9656 000 111</a>
                        <a href="mailto:info@bigspaceok.com"><i class="fa fa-envelope"></i>info@bigspaceok.com</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <ul class="top-social-media pull-right">
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="user/logout" class="sign-in"><i class="fa fa-sign-in"></i> Logout</a>
                        @else
                            <li>
                                <a href="user/login" class="sign-in"><i class="fa fa-sign-in"></i> Login</a>
                            </li>
                            <!--<li>
                                <a href="signup.html" class="sign-in"><i class="fa fa-user"></i> Register</a>
                            </li>-->
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Main header start -->
    <header class="main-header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo">
                        <img src="img/logos/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/sales">Sales</a></li>
                        <li><a href="/rentals">Rentals</a></li>
                        <li><a href="/projects">Projects</a></li>
                        <li><a href="/blogs">News/Blog</a></li>
                        <li><a href="/moments">Moments</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right rightside-navbar">
                        <li>
                            <a href="/submit-property" class="button">
                                Submit Property
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->

    <!-- Banner start -->
    <div class="blog-banner">
        <div class="mask-blog">
            <div class="container">
                <div class="breadcrumb-area">
                    <h1>Contact Us</h1>
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner end -->

    <!-- Submit Property start -->
    <div class="content-area submit-property">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if (\Session::has('message'))
                       <div id="success-alert" class="alert alert-success wow fadeInLeft delay-03s"  role="alert">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <strong>Thank You!</strong> We contact you shortly.
                       </div>
                       <script>
                            $("#success-alert").fadeTo(4500, 500).slideUp(500, function(){
                                $("#success-alert").slideUp(500);
                            });
                       </script>
                    @endif
                    @if (\Session::has('error'))
                        <div id="error-alert" class="alert alert-danger wow fadeInRight delay-03s"  role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Oh snap!</strong> Change a few things up and try
                            submitting again.
                        </div>
                        <script>
                            $("#error-alert").fadeTo(4500, 500).slideUp(500, function(){
                                $("#error-alert").slideUp(500);
                            });
                        </script>
                    @endif
                </div>


                <div class="col-md-12">
                    <div class="submit-address">
                        <form method="post" action="/submit-property">
                            {!! csrf_field() !!}
                            <!-- basic information start -->
                            <div class="main-title-2">
                                <h1><span>Basic</span> Information</h1>
                                <small class="heading heading-double-icon myhead"><span>&nbsp;</span></small>
                            </div>
                            <div class="search-contents-sidebar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Title</label>
                                            <input type="text" class="input-text" name="name" placeholder="Property Title" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select name="status" class="selectpicker search-fields" required="">
                                                <option>For Rent</option>
                                                <option>For Sale</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="text" class="input-text" name="price" placeholder="USD">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Area/Location</label>
                                            <input type="text" class="input-text" name="area" placeholder="SqFt" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- basic information end -->


                            <!-- building location address start -->
                            <div class="main-title-2">
                                <h1><span>Location</span></h1>
                                <small class="heading heading-double-icon myhead"><span>&nbsp;</span></small>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="input-text" name="address"  placeholder="Address" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="input-text" name="city"  placeholder="City">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>District</label>
                                        <select name="district" class="form-control">
                                            <option>Choose District</option>
                                             <option  ng-repeat="district in districts" value="@{{district.id}}">@{{district.name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>State</label>
                                        <select name="state" class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value" ng-model="state_id" ng-change="getDistrict(state_id);" required="">
                                            <option value="">Choose State</option>
                                            @if(isset($states))
                                                @foreach($states as $state)
                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Postal Code</label>
                                        <input  type="text" class="input-text" name="pincode"  placeholder="Postal Code">
                                    </div>
                                </div>
                            </div>
                            <!-- building location address end -->

                            <!-- contact details start -->
                            <div class="main-title-2">
                                <h1><span>Contact</span> Details</h1>
                                <small class="heading heading-double-icon myhead"><span>&nbsp;</span></small>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="input-text" name="contactname" placeholder="Name" required="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="input-text" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type="text" class="input-text" name="phone"  placeholder="Phone" required="">
                                    </div>
                                </div>
                            </div>
                            <!-- contact details  end -->

                            <!-- property image add start -->
                            <div class="main-title-2">
                                <h1><span>Property</span> Gallery</h1>
                                <small class="heading heading-double-icon myhead"><span>&nbsp;</span></small>
                            </div>
                            <div id="myDropZone" class="dropzone dropzone-design">
                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                            </div>
                            <!--<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span class="btn btn-default btn-file">
                                            Browse <input type="file" name="photos" id="imgInp" multiple>
                                        </span>
                                    </div>
                                    <br/>
                                </div>
                            </div>-->

                            <!-- property image add end -->

                            <!-- detailed desciption start -->
                            <div class="main-title-2">
                                <h1><span>Detailed</span> Information</h1>
                                <small class="heading heading-double-icon myhead"><span>&nbsp;</span></small>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Detailed Information</label>
                                        <textarea class="input-text" name="description" placeholder="Detailed Information"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- detailed description end -->


                            <div class="col-md-12">
                                <div class="button-section pull-right">
                                    <a href="#" class="btn button-md border-button-theme">Cancel</a>
                                    <button type="submit" class="btn button-md button-theme">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Submit Property end -->

    <!-- Footer start -->
    <footer class="main-footer clearfix">
        <div class="container">
            <!-- Footer top -->
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
                        <div class="logo-2">
                            <a href="/">
                                <img src="img/logos/footer-logo.png" alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                        <form action="#" method="post">
                            <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                            <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-list clearfix">
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Footer info-->
            <div class="footer-info">
                <div class="row">
                    <!-- About us -->
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Contact Us</h1>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's printing and typesetting
                            </p>
                            <ul class="personal-info">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    Address: 44 New Design Street, Melbourne
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    Email:<a href="mailto:sales@hotelempire.com">sales@bigspaceok.com</a>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    Phone: <a href="tel:+55-417-634-7071">+55 417-634-7071</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Links -->
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Quik Links</h1>
                            </div>
                            <ul class="links">
                                <li>
                                    <a href="/">Home</a>
                                </li>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="/sales">Sales Properties</a>
                                </li>
                                <li>
                                    <a href="/rentals">Rental Properties</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Tags -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-item tags-box">
                            <div class="main-title-2">
                                <h1>Big Space In</h1>
                            </div>
                            <ul class="tags">
                                <li><a href="#">Manjeri</a></li>
                                <li><a href="#">Malappuram</a></li>
                                <li><a href="#">Calicut</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Recent cars -->
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                        <div class="footer-item popular-posts">
                            <div class="main-title-2">
                                <h1>Scan With</h1>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object qr" src="img/qr.png" alt="qr">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Copy right start -->
    <div class="copy-right">
        <div class="container">
            We are Big Spcae Ok. All rights reserved &copy; <span id="year"></span> Made by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies</a>
        </div>
    </div>
    <!-- Copy end right-->


    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{url('js/rangeslider.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mb.YTPlayer.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{url('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>

    <script type="text/javascript" src="{{url('js/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/myangularapp.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <!-- Custom javascript -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-100829780-3', 'auto');
         ga('send', 'pageview');
    </script>
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>
</body>
</html>