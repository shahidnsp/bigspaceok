<!DOCTYPE html>
<html lang="en">
<head>
    <title>Big Space OK | Our Projects</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="author" content="Psybo Technologies">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/leaflet.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/map.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/dropzone.css')}}">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{url('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{url('css/ie10-viewport-bug-workaround.css')}}">
     <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
     <script type="text/javascript" src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
</head>

<body>
    <div class="page_loader"></div>

    <!-- Top header start -->
    <header class="top-header hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="list-inline">
                        <a href="tel:+919656000111" target="_blank"><i class="fa fa-phone"></i>+91 9656 000 111</a>
                        <a href="tel:info@bigspaceok.com"><i class="fa fa-envelope"></i>info@bigspaceok.com</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <ul class="top-social-media pull-right">
                       @if(\Illuminate\Support\Facades\Auth::check())
                           <a href="user/logout" class="sign-in"><i class="fa fa-sign-in"></i> Logout</a>
                       @else
                           <li>
                               <a href="user/login" class="sign-in"><i class="fa fa-sign-in"></i> Login</a>
                           </li>
                           <!--<li>
                               <a href="signup.html" class="sign-in"><i class="fa fa-user"></i> Register</a>
                           </li>-->
                       @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Main header start -->
    <header class="main-header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo">
                        <img src="img/logos/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/sales">Sales</a></li>
                        <li><a href="/rentals">Rentals</a></li>
                        <li class="active"><a href="/projects">Projects</a></li>
                        <li><a href="/blogs">News/Blog</a></li>
                        <li><a href="/gallery">Moments</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right rightside-navbar">
                        <li>
                            <a href="/submit-property" class="button">
                                Submit Property
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->

    <!-- Banner start -->
    <div class="blog-banner">
        <div class="mask-blog">
            <div class="container">
                <div class="breadcrumb-area">
                    <h1>Our Projects</h1>
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a></li>
                        <li class="active">Our Projects</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner end -->

    <!-- Properties section body start -->
    <div class="properties-section property-big content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    @foreach($projects as $project)
                        <!-- property start -->
                        <div class="property clearfix wow fadeInUp delay-03s">
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 col-pad">
                                <a href="/single-project/{{$project->id}}" class="property-img height">
                                    <img src="images/{{$project->photos->first()['name']}}" alt="properties-3" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 property-content ">
                                <!-- title -->
                                <h1 class="title">
                                    <a href="#">{{$project->name}}</a>
                                </h1>
                                <!-- Property address -->
                                <h3 class="property-address">
                                    <a href="#">
                                        <i class="fa fa-map-marker"></i>{{$project->place}},
                                    </a>
                                </h3>
                                <p>
                                    {!! str_limit($project->description, $limit = 150, $end = '...') !!}
                                </p>
                                <!-- Property footer -->
                                <div class="property-footer">
                                    <!--<span class="left"><i class="fa fa-calendar-o icon"></i> 10/12/1993</span>-->
                                    <span class="right">
                                        <a href="/single-project/{{$project->id}}" class="btn button-sm border-button-theme">Details</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- property end -->
                    @endforeach

                    <!-- Page navigation start -->
                    <nav aria-label="Page navigation">
                        {!! $projects->render() !!}
                    </nav>
                    <!-- Page navigation end-->
                </div>
            </div>
        </div>
    </div>
    <!-- Properties section body end -->

    <!-- Partners block start -->
    <div class="partners-block">
        <div class="container">
            <h3>Brands & Partners</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel our-partners slide" id="ourPartners">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="img/brand/audiojungle.png" alt="audiojungle">
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="img/brand/themeforest.png" alt="themeforest">
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="img/brand/tuts.png" alt="tuts">
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="img/brand/graphicriver.png" alt="graphicriver">
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="img/brand/codecanyon.png" alt="codecanyon">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                        <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partners block end -->

    <!-- Footer start -->
    <footer class="main-footer clearfix">
        <div class="container">
            <!-- Footer top -->
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
                        <div class="logo-2">
                            <a href="/">
                                <img src="img/logos/footer-logo.png" alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                        <form action="#" method="post">
                            <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                            <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-list clearfix">
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Footer info-->
            <div class="footer-info">
                <div class="row">
                    <!-- About us -->
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Contact Us</h1>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's printing and typesetting
                            </p>
                            <ul class="personal-info">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    Address: 44 New Design Street, Melbourne
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    Email:<a href="mailto:sales@hotelempire.com">sales@bigspaceok.com</a>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    Phone: <a href="tel:+55-417-634-7071">+55 417-634-7071</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Links -->
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Quik Links</h1>
                            </div>
                            <ul class="links">
                                <li>
                                    <a href="/">Home</a>
                                </li>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="/sales">Sales Properties</a>
                                </li>
                                <li>
                                    <a href="/rentals">Rental Properties</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Tags -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-item tags-box">
                            <div class="main-title-2">
                                <h1>Big Space In</h1>
                            </div>
                            <ul class="tags">
                                <li><a href="#">Manjeri</a></li>
                                <li><a href="#">Malappuram</a></li>
                                <li><a href="#">Calicut</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Recent cars -->
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                        <div class="footer-item popular-posts">
                            <div class="main-title-2">
                                <h1>Scan With</h1>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object qr" src="img/qr.png" alt="qr">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Copy right start -->
    <div class="copy-right">
        <div class="container">
            We are Big Spcae Ok. All rights reserved &copy; <span id="year"></span> Made by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies</a>
        </div>
    </div>
    <!-- Copy end right-->

    <script type="text/javascript" src="{{url('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{url('js/rangeslider.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mb.YTPlayer.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{url('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <!-- Custom javascript -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-100829780-3', 'auto');
         ga('send', 'pageview');
    </script>
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>
</body>
</html>