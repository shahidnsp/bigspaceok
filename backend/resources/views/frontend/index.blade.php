<!DOCTYPE html>
<html lang="en" ng-app="myWeb">
<head>
    <title>Big Space OK | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="author" content="Psybo Technologies">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/leaflet.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/map.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/dropzone.css')}}">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{url('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{url('css/ie10-viewport-bug-workaround.css')}}">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script type="text/javascript" src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery-2.2.0.min.js')}}"></script>
</head>



<body ng-controller="HomeController">
    @if (\Session::has('message'))
       <div id="success-alert" class="alert alert-success text-center"><h1>{{ \Session::get('message') }}</h1></div>
       <script>
            $("#success-alert").fadeTo(3500, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
       </script>
    @endif
    <div class="page_loader"></div>

    <!-- Top header start -->
    <header class="top-header hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="list-inline">
                        <a href="tel:+919656000111" target="_blank"><i class="fa fa-phone"></i>+91 9656 000 111</a>
                        <a href="tel:info@bigspaceok.com"><i class="fa fa-envelope"></i>info@bigspaceok.com</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <ul class="top-social-media pull-right">
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="user/logout" class="sign-in"><i class="fa fa-sign-in"></i> Logout</a>
                        @else
                            <li>
                                <a href="user/login" class="sign-in"><i class="fa fa-sign-in"></i> Login</a>
                            </li>
                            <!--<li>
                                <a href="signup.html" class="sign-in"><i class="fa fa-user"></i> Register</a>
                            </li>-->
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Main header start -->
    <header class="main-header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo">
                        <img src="img/logos/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">Home</a></li>
                        <li><a href="/sales">Sales</a></li>
                        <li><a href="/rentals">Rentals</a></li>
                        <li><a href="/projects">Projects</a></li>
                        <li><a href="/blogs">News/Blog</a></li>
                        <li><a href="/moments">Moments</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right rightside-navbar">
                        <li>
                            <a href="/submit-property" class="button">
                                Submit Property
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->

    <!-- Banner start -->
    <div class="banner">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="img/banner/banner-slider-1.jpg" alt="banner-slider-1">
                    <div class="carousel-caption banner-slider-inner banner-top-align"></div>
                </div>
                <div class="item">
                    <img src="img/banner/banner-slider-2.jpg" alt="banner-slider-2">
                    <div class="carousel-caption banner-slider-inner banner-top-align"></div>
                </div>
                <div class="item">
                    <img src="img/banner/banner-slider-3.jpg" alt="banner-slider-3">
                    <div class="carousel-caption banner-slider-inner banner-top-align"></div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="slider-mover-left" aria-hidden="true">
                    <i class="fa fa-angle-left"></i>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="slider-mover-right" aria-hidden="true">
                    <i class="fa fa-angle-right"></i>
                </span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- Banner end -->




    <div class="overflow">
        <div class="row">
            <div class="col-md-12">
                <div id="Carousel" class="carousel slide">
                    <ol class="carousel-indicators my-indicators">
                        <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#Carousel" data-slide-to="1"></li>
                        <li data-target="#Carousel" data-slide-to="2"></li>
                    </ol>


                    <div class="carousel-inner">

                   <?php $moments=\App\Moment::all();
                        $length=count($moments);
                   ?>
                    @if(isset($moments))
                       @if($length<=4)
                           <div class="item active">
                               <div class="row">
                                @for($i=0;$i<$length;$i++)
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[$i]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                @endfor
                               </div>
                           </div>
                       @else
                           <div class="item active">
                                <div class="row">
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[0]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[1]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[2]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[3]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                </div>
                           </div>
                       @endif

                       @if($length>4 && $length<8)
                            <div class="item">
                               <div class="row">
                                @for($i=4;$i<$length;$i++)
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[$i]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                @endfor
                               </div>
                           </div>
                       @else
                            <div class="item">
                                <div class="row">
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[4]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[5]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[6]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                   <div class="col-md-3 my-pad">
                                       <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                           <img src="images/{{$moments[7]->photo}}" alt="Image" style="max-width:100%;">
                                       </a>
                                   </div>
                                </div>
                           </div>
                       @endif

                        @if($length>8 && $length<12)
                           <div class="item">
                              <div class="row">
                               @for($i=8;$i<$length;$i++)
                                  <div class="col-md-3 my-pad">
                                      <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                          <img src="images/{{$moments[$i]->photo}}" alt="Image" style="max-width:100%;">
                                      </a>
                                  </div>
                               @endfor
                              </div>
                          </div>
                        @else
                           <div class="item">
                               <div class="row">
                                  <div class="col-md-3 my-pad">
                                      <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                          <img src="images/{{$moments[8]->photo}}" alt="Image" style="max-width:100%;">
                                      </a>
                                  </div>
                                  <div class="col-md-3 my-pad">
                                      <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                          <img src="images/{{$moments[9]->photo}}" alt="Image" style="max-width:100%;">
                                      </a>
                                  </div>
                                  <div class="col-md-3 my-pad">
                                      <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                          <img src="images/{{$moments[10]->photo}}" alt="Image" style="max-width:100%;">
                                      </a>
                                  </div>
                                  <div class="col-md-3 my-pad">
                                      <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                          <img src="images/{{$moments[11]->photo}}" alt="Image" style="max-width:100%;">
                                      </a>
                                  </div>
                               </div>
                          </div>
                        @endif
                    @endif

                    </div>
                      <a data-slide="prev" href="#Carousel" class="left carousel-control h-control">‹</a>
                      <a data-slide="next" href="#Carousel" class="right carousel-control h-control">›</a>
                </div><!--.Carousel-->
            </div>
        </div>
    </div><!--.container-->

    <!-- Search area start -->
    <div class="search-area">
        <div class="container">
            <div class="search-area-inner">
                <div class="search-contents ">
                    <div class="row">
                        <form method="get" action="/search">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <?php $states=\App\State::select('id','name')->get(); ?>
                                    <select class="selectpicker search-fields" name="state_id" ng-model="state_id" ng-change="getDistrict(state_id);">
                                            <option value="">Select State</option>
                                        @foreach($states as $state)
                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <select class="form-control" name="district_id" id="myselect">
                                        <option value="">District</option>
                                        <option  ng-repeat="district in districts" value="@{{district.id}}">@{{district.name}}</option>
                                    </select>
                                </div>
                            </div>
                           <!--  <script>
                                function reverseGeocodeAddress() {
                                    $.ajax({
                                        type: "GET",
                                        url: './getDistricts?state_id=13',
                                        data: "",
                                        success: function(response) {
                                            $.each(response, function(i, value) {
                                                        $('#myselect').append($('<option>').text(value.name).attr('value', value.id));
                                                        //console.log(value.name);
                                                    });
                                        }
                                    })
                                };
                            </script> -->
                            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <select>
                                        <option>Location</option>
                                        <option>Location 1</option>
                                        <option>Location 2</option>
                                        <option>Location 3</option>
                                        <option>Location 4</option>
                                        <option>Location 5</option>
                                        <option>Location 6</option>
                                        <option>Location 7</option>
                                    </select>
                                </div>
                            </div>-->
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 ">
                                <div class="form-group">
                                    <button type="submit" class="search-button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search area start -->



    <!-- Featured properties start -->
    <div class="content-area featured-properties">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1><span>Our Featured</span> Properties</h1>
                <small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-map-marker"></i>
                    <span>&nbsp;</span>
                </small>
            </div>
            <div class="row">
                <div class="filtr-container">
                    <?php $features=\App\FeaturedProperty::with('property.photos')->get() ?>
                    @if(isset($features))
                        @foreach($features as $feature)
                            @if(isset($feature->property))
                            <!-- one property start -->
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                                <div class="property">
                                    <!-- Property img -->
                                    <a class="property-img">
                                        <div class="property-tag button alt featured">Verified</div>
                                        <div class="property-tag button sale">{{$feature->property->status}}</div>
                                        <div class="property-price"><i class="fa fa-inr"></i>{{$feature->property->price}}</div>
                                        @if(isset($feature->property->photos->first()->name))<img src="images/{{$feature->property->photos->first()->name}}" alt="properties-1" class="img-responsive">@endif
                                    </a>
                                    <!-- Property content -->
                                    <div class="property-content">
                                        <!-- title -->
                                        <h1 class="title">
                                            <a>{{$feature->property->name}}</a>
                                        </h1>
                                        <ul class="facilities-list clearfix">
                                            <li><i class="fa fa-map-marker"></i><span>{{$feature->property->city}}</span></li>
                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i>
                                                <span>{{$feature->property->area}} sq ft</span>
                                            </li>
                                            <li><a href="tel:+919656000111" target="_blank" class="callbutton">Call Now</a></li>
                                            <li><a href="#" class="detailsbutton" data-toggle="modal" data-target="#myModal">More Details</a></li>
                                        </ul>
                                        <!-- Property footer -->
                                        <div class="property-footer">
                                            <span class="left"><i class="fa fa-calendar-o icon"></i> &nbsp;<?php $now=\Carbon\Carbon::now(); $date=\Carbon\Carbon::parse($feature->property->created_at); $length=$date->diffInDays($now); if($length==0) $length=1; echo $length; ?> days ago</span>
                                            <span class="right">
                                                <a href="whatsapp://send?text=Search Rented Building from http://bigspaceok.com" target="_blank"><i class="fa fa-whatsapp mr10"></i></a>
                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{url()}}&display=popup" target="_blank"><i class="fa fa-facebook mr10"></i></a>
                                                <a href="#"><i class="fa fa-share-alt mr10"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- one property end -->
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Featured properties end -->

    <!-- Listings parallax start -->
    <div class="listings-parallax clearfix mrg-btm-70">
        <div class="listings-parallax-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-8 col-xs-12">
                        <h1>We are Here to Help You ...</h1>
                        <h3>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsa necsagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</h3>
                        <a href="#" class="btn button-sm button-theme">Contact Now</a>
                    </div>
                    <div class="col-lg-offset-3 col-lg-3 col-sm-4 col-xs-12">
                        <div class="contect-agent-photo">
                            <img src="img/avatar/avatar-8.png" alt="avatar-6" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Listings parallax end -->

    <!-- Intro section strat -->
    <div class="intro-section mrg-btm-70">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <img src="img/logos/logo-2.png" alt="logo-2">
                </div>
                <div class="col-md-7 col-sm-6 col-xs-12">
                    <p>Big Space Ok is entirely an innovative conception that has been based on a foresightedness inspiration.</p>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12">
                    <a href="#" class="btn button-md button-theme hidden-xs">Contact now</a>
                    <a href="#" class="btn button-sm button-theme hidden-lg hidden-md hidden-sm">Contact now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Intro section end -->

    <div class="clearfix"></div>

    <!-- Categories strat -->
    <div class="categories">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1><span>Popular</span> Places</h1>
                <small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-location-arrow"></i>
                    <span>&nbsp;</span>
                </small>
            </div>
            <div class="clearfix"></div>
            <div class="row wow">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-pad wow fadeInLeft delay-04s">
                            <div class="category">
                                <div class="category_bg_box cat-1-bg">
                                    <div class="category-overlay">
                                        <span class="category-content">
                                            <span class="category-title">Florida</span>
                                            <br>
                                            <span class="category-subtitle">14 Properties</span>
                                            <br>
                                            <a href="#" class="btn button-sm button-theme">View All</a>
                                        </span><!-- /.category-content -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-pad wow fadeInLeft delay-04s">
                            <div class="category">
                                <div class="category_bg_box cat-2-bg">
                                    <div class="category-overlay">
                                        <span class="category-content">
                                            <span class="category-title">California</span>
                                            <br>
                                            <span class="category-subtitle">14 Properties</span>
                                            <br>
                                             <a href="#" class="btn button-sm button-theme ">View All</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-pad wow fadeInUp delay-04s">
                            <div class="category">
                                <div class="category_bg_box cat-3-bg">
                                    <div class="category-overlay">
                                        <span class="category-content">
                                            <span class="category-title">New York</span>
                                            <br>
                                            <span class="category-subtitle">27 Properties</span>
                                            <br>
                                            <a href="#" class="btn button-sm button-theme ">View All</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-12 col-pad wow fadeInRight delay-04s">
                    <div class="category">
                        <div class="category_bg_box category_long_bg cat-4-bg">
                            <div class="category-overlay">
                                <span class="category-content">
                                    <span class="category-title">San Francisco</span>
                                    <br>
                                    <span class="category-subtitle">14 Properties</span>
                                    <br>
                                     <a href="#" class="btn button-sm button-theme ">View All</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Categories end-->

    <!-- What are you looking for? strat -->
    <div class="our-service our-service-two mrg-btm-70">
        <div class="our-service-inner">
            <div class="container">
                <!-- Main title -->
                <div class="main-title">
                    <h1><span>What Are you </span> Looking For?</h1>
                </div>

                <div class="row mrg-btm-30 wow animated" style="visibility: visible;">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInLeft delay-04s" style="visibility: visible; animation-name: fadeInLeft;">
                        <div class="content">
                            <i class="flaticon-apartment"></i>
                            <h4>Apartments</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna . </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInLeft delay-04s" style="visibility: visible; animation-name: fadeInLeft;">
                        <div class="content">
                            <i class="flaticon-internet"></i>
                            <h4>Government services</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna . </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInRight delay-04s" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="content">
                            <i class="flaticon-vehicle"></i>
                            <h4>Garages</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna . </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInRight delay-04s" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="content">
                            <i class="flaticon-symbol"></i>
                            <h4>Commercial</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna . </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- What are you looking for? strat -->

    <!-- Customer say start -->
    <div class="mrg-btm-70 customer-say chevron-icon">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1><span>What </span>our clients say</h1>
                <small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-lightbulb-o"></i>
                    <span>&nbsp;</span>
                </small>
            </div>
            <div class="row">
                <div class="carousel our-partners slide" id="ourPartners4">
                    <div class="col-lg-12 mrg-btm-30">
                        <a class="right carousel-control" href="#ourPartners4" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                        <a class="right carousel-control" href="#ourPartners4" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                    </div>
                    <div class="carousel-inner">

                        <?php
                            $testimonials=\App\Testimonial::all();
                        ?>
                        @if(isset($testimonials))
                            @foreach($testimonials as $key=>$testimonial)
                                @if($key==0)
                                    <div class="item active">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <!-- Customers say box start-->
                                            <div class="customers-say-box">
                                                <!--header -->
                                                <div class="header clearfix">
                                                    <h2>{{$testimonial->name}}</h2>
                                                    <h4>{{$testimonial->designation}}</h4>
                                                    <img src="images/{{$testimonial->photo}}" alt="avatar-1" class="profile-img">
                                                </div>
                                                <!-- Detail -->
                                                <div class="detail clearfix">
                                                    <p>{{$testimonial->content}}</p>
                                                </div>
                                            </div>
                                            <!-- Customers say box end-->
                                        </div>
                                    </div>
                                @else
                                    <div class="item">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <!-- Customers say box start-->
                                            <div class="customers-say-box">
                                                <!--header -->
                                                <div class="header clearfix">
                                                    <h2>{{$testimonial->name}}</h2>
                                                    <h4>{{$testimonial->designation}}</h4>
                                                    <img src="images/{{$testimonial->photo}}" alt="avatar-1" class="profile-img">
                                                </div>
                                                <!-- Detail -->
                                                <div class="detail clearfix">
                                                    <p>{{$testimonial->content}}</p>
                                                </div>
                                            </div>
                                            <!-- Customers say box end-->
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Customer say end -->
    <div class="clearfix"></div>

    <!-- Counters strat -->
    <div class="counters">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 bordered-right">
                    <div class="counter-box">
                        <i class="flaticon-tag"></i>
                        <h1 class="counter">967</h1>
                        <p>Listings For Sale</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 bordered-right">
                    <div class="counter-box">
                        <i class="flaticon-symbol-1"></i>
                        <h1 class="counter">1276</h1>
                        <p>Listings For Rent</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 bordered-right">
                    <div class="counter-box">
                        <i class="flaticon-people"></i>
                        <h1 class="counter">396</h1>
                        <p>Agents</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter-box">
                        <i class="flaticon-people-1"></i>
                        <h1 class="counter">177</h1>
                        <p>Brokers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counters end -->

    <!-- Partners block start -->
    <div class="partners-block">
        <div class="container">
            <h3>Brands & Partners</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel our-partners slide" id="ourPartners">
                        <div class="carousel-inner">
                        <?php
                            $clients=\App\Client::all();
                         ?>
                         @if(isset($clients))
                             @foreach($clients as $key=> $client)
                                    @if($key==0)
                                    <div class="item active">
                                        <div class="col-xs-12 col-sm-6 col-md-3">
                                            <a href="#">
                                                <img src="images/{{$client->logo}}" alt="logo">
                                            </a>
                                        </div>
                                    </div>
                                    @else
                                        <div class="item">
                                            <div class="col-xs-12 col-sm-6 col-md-3">
                                                <a href="#">
                                                    <img src="images/{{$client->logo}}" alt="logo">
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                              @endforeach
                          @endif
                        </div>
                        <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                        <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partners block end -->

    <!-- Footer start -->
    <footer class="main-footer clearfix">
        <div class="container">
            <!-- Footer top -->
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
                        <div class="logo-2">
                            <a href="/">
                                <img src="img/logos/footer-logo.png" alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                        <form action="#" method="post">
                            <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                            <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-list clearfix">
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Footer info-->
            <div class="footer-info">
                <div class="row">
                    <!-- About us -->
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Contact Us</h1>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's printing and typesetting
                            </p>
                            <ul class="personal-info">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    Address: 44 New Design Street, Melbourne
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    Email:<a href="mailto:sales@hotelempire.com">sales@bigspaceok.com</a>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    Phone: <a href="tel:+55-417-634-7071">+55 417-634-7071</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Links -->
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Quik Links</h1>
                            </div>
                            <ul class="links">
                                <li>
                                    <a href="/">Home</a>
                                </li>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="/sales">Sales Properties</a>
                                </li>
                                <li>
                                    <a href="/rentals">Rental Properties</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Tags -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-item tags-box">
                            <div class="main-title-2">
                                <h1>Big Space In</h1>
                            </div>
                            <ul class="tags">
                                <li><a href="#">Manjeri</a></li>
                                <li><a href="#">Malappuram</a></li>
                                <li><a href="#">Calicut</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Recent cars -->
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                        <div class="footer-item popular-posts">
                            <div class="main-title-2">
                                <h1>Scan With</h1>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object qr" src="img/qr.png" alt="qr">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- fancybox modal start -->
    <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" alt="" />
                </div>
            </div>
        </div>
    </div>
    <!-- fancybox modal end -->

    <!-- Copy right start -->
    <div class="copy-right">
        <div class="container">
            We are Big Spcae Ok. All rights reserved &copy; <span id="year"></span> Made by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies</a>
        </div>
    </div>
    <!-- Copy end right-->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Request Now</h3>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form method="post" action="/getQuotes">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Enter Your Name" required="">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input name="email" type="email" class="form-control" id="email" placeholder="Enter Your Email">
                        </div>
                        <div class="form-group">
                            <label for="Number">Contact Number 1</label>
                            <input name="number1" type="text" class="form-control" id="Number" placeholder="Your Contact Number" required="">
                        </div>
                        <div class="form-group">
                            <label for="Number2">Contact Number 2</label>
                            <input name="number2" type="text" class="form-control" id="Number2" placeholder="Your Contact Number">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Place</label>
                            <input name="place" type="text" class="form-control" id="exampleInputPassword1" placeholder="Your Place" required="">
                        </div>
                        <div class="button-section pull-right">
                            <!--<a href="#" class="btn button-md border-button-theme">Cancel</a>-->
                            <button class="btn button-md border-button-theme" type="button" class="close" data-dismiss="modal">Cancel</button>
                            <button class="btn button-md button-theme" type="submit">Submit</button>
                            <!--<a href="#" class="btn button-md button-theme">Sumbit</a>-->
                        </div>
                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{url('js/rangeslider.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mb.YTPlayer.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{url('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>
    <script type="text/javascript" src="{{url('js/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/myangularapp.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <!-- Custom javascript -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-100829780-3', 'auto');
         ga('send', 'pageview');

         $(document).ready(function() {
            $('#Carousel').carousel({
                interval: 5000
            })
        });

        $(document).ready(function() {
            var $lightbox = $('#lightbox');

            $('[data-target="#lightbox"]').on('click', function(event) {
                var $img = $(this).find('img'),
                    src = $img.attr('src'),
                    alt = $img.attr('alt'),
                    css = {
                        'maxWidth': $(window).width() - 100,
                        'maxHeight': $(window).height() - 100
                    };

                $lightbox.find('.close').addClass('hidden');
                $lightbox.find('img').attr('src', src);
                $lightbox.find('img').attr('alt', alt);
                $lightbox.find('img').css(css);
            });

            $lightbox.on('shown.bs.modal', function (e) {
                var $img = $lightbox.find('img');

                $lightbox.find('.modal-dialog').css({'width': $img.width()});
                $lightbox.find('.close').removeClass('hidden');
            });
        });
    </script>
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>
</body>
</html>