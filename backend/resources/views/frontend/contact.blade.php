<!DOCTYPE html>
<html lang="en">
<head>
    <title>Big Space OK | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="author" content="Psybo Technologies">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/leaflet.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/map.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/dropzone.css')}}">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{url('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
     <link rel="stylesheet" type="text/css" href="{{url('css/ie10-viewport-bug-workaround.css')}}">
     <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
     <script type="text/javascript" src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
      <script type="text/javascript" src="{{url('js/jquery-2.2.0.min.js')}}"></script>
</head>

<body>
     @if (\Session::has('message'))
       <div id="success-alert" class="alert alert-success text-center"><h1>{{ \Session::get('message') }}</h1></div>
       <script>
            $("#success-alert").fadeTo(3500, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
       </script>
     @endif
    <div class="page_loader"></div>

    <!-- Top header start -->
    <header class="top-header hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="list-inline">
                        <a href="tel:+919656000111" target="_blank"><i class="fa fa-phone"></i>+91 9656 000 111</a>
                        <a href="tel:info@bigspaceok.com"><i class="fa fa-envelope"></i>info@bigspaceok.com</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <ul class="top-social-media pull-right">
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="user/logout" class="sign-in"><i class="fa fa-sign-in"></i> Logout</a>
                        @else
                            <li>
                                <a href="user/login" class="sign-in"><i class="fa fa-sign-in"></i> Login</a>
                            </li>
                            <!--<li>
                                <a href="signup.html" class="sign-in"><i class="fa fa-user"></i> Register</a>
                            </li>-->
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Main header start -->
    <header class="main-header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="logo">
                        <img src="img/logos/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/sales">Sales</a></li>
                        <li><a href="/rentals">Rentals</a></li>
                        <li><a href="/projects">Projects</a></li>
                        <li><a href="/blogs">News/Blog</a></li>
                        <li><a href="/moments">Moments</a></li>
                        <li class="active"><a href="/contact">Contact Us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right rightside-navbar">
                        <li>
                            <a href="/submit-property" class="button">
                                Submit Property
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->

    <!-- Banner start -->
    <div class="blog-banner">
        <div class="mask-blog">
            <div class="container">
                <div class="breadcrumb-area">
                    <h1>Contact Us</h1>
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner end -->

    <!-- Contact body start -->
    <div class="contact-body content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <!-- Contact form start -->
                    <div class="contact-form">
                        <div class="main-title-2">
                            <h1><span>Contact</span> with us</h1>
                        </div>
                        <form id="contact_form" action="/getintouch" method="POST">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group fullname">
                                        <input type="text" name="name" class="input-text" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group enter-email">
                                        <input type="email" name="email" class="input-text" placeholder="Enter email">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group subject">
                                        <input type="text" name="subject" class="input-text" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group number">
                                        <input type="text" name="phone" class="input-text" placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                    <div class="form-group message">
                                        <textarea class="input-text" name="message" placeholder="Write message"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group send-btn">
                                        <button type="submit" class="button-md button-theme">Send Message</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Contact form end -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <!-- Contact details start -->
                    <div class="contact-details">
                        <div class="main-title-2">
                            <h1><span>Our</span> address</h1>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="media-body">
                                <h4>Office Address</h4>
                                <p>Lorem Ipsum is simply dummy text printing and type setting industry 5562. po alpu</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="media-body">
                                <h4>Phone Number</h4>
                                <p>
                                    <a href="tel:0477-0477-8556-552">office: 0477 8556 552</a>
                                </p>
                                <p>
                                    <a href="tel:+55-417-634-7071">Mobile: +55 417 634 7071</a>
                                </p>
                            </div>
                        </div>
                        <div class="media mrg-btm-0">
                            <div class="media-left">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="media-body">
                                <h4>Email Address</h4>
                                <p>
                                    <a href="mailto:info@themevessel.com">info@themevessel.com</a>
                                </p>
                                <p>
                                    <a href="http://themevessel.com/" target="_blank">http://themevessel.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Contact details end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact body end -->

    <!-- Google map start -->
    <div class="section">
        <div class="map">
            <div id="map" class="contact-map"></div>
        </div>
    </div>
    <!-- Google map end -->

    <!-- Footer start -->
    <footer class="main-footer clearfix">
        <div class="container">
            <!-- Footer top -->
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
                        <div class="logo-2">
                            <a href="/">
                                <img src="img/logos/footer-logo.png" alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                        <form action="#" method="post">
                            <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                            <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                                <i class="fa fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <ul class="social-list clearfix">
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Footer info-->
            <div class="footer-info">
                <div class="row">
                    <!-- About us -->
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Contact Us</h1>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's printing and typesetting
                            </p>
                            <ul class="personal-info">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    Address: 44 New Design Street, Melbourne
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    Email:<a href="mailto:sales@hotelempire.com">sales@bigspaceok.com</a>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    Phone: <a href="tel:+55-417-634-7071">+55 417-634-7071</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Links -->
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>Quik Links</h1>
                            </div>
                            <ul class="links">
                                <li>
                                    <a href="/">Home</a>
                                </li>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="/sales">Sales Properties</a>
                                </li>
                                <li>
                                    <a href="/rentals">Rental Properties</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Tags -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-item tags-box">
                            <div class="main-title-2">
                                <h1>Big Space In</h1>
                            </div>
                            <ul class="tags">
                                <li><a href="#">Manjeri</a></li>
                                <li><a href="#">Malappuram</a></li>
                                <li><a href="#">Calicut</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                                <li><a href="#">Another</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Recent cars -->
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                        <div class="footer-item popular-posts">
                            <div class="main-title-2">
                                <h1>Scan With</h1>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object qr" src="img/qr.png" alt="qr">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Copy right start -->
    <div class="copy-right">
        <div class="container">
            We are Big Spcae Ok. All rights reserved &copy; <span id="year"></span> Made by <a href="http://psybotechnologies.com/" target="_blank">Psybo Technologies</a>
        </div>
    </div>
    <!-- Copy end right-->


    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{url('js/rangeslider.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mb.YTPlayer.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{url('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <!-- Custom javascript -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-100829780-3', 'auto');
         ga('send', 'pageview');
      </script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>
    <script>
        function LoadMap(propertes) {
            var defaultLat = 40.7110411;
            var defaultLng = -74.0110326;
            var mapOptions = {
                center: new google.maps.LatLng(defaultLat, defaultLng),
                zoom: 15,
                scrollwheel: false,
                styles: [
                    {
                        featureType: "administrative",
                        elementType: "labels",
                        stylers: [
                            {visibility: "off"}
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels",
                        stylers: [
                            {visibility: "off"}
                        ]
                    },
                    {
                        featureType: 'poi.business',
                        stylers: [{visibility: 'off'}]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'labels.icon',
                        stylers: [{visibility: 'off'}]
                    },
                ]
            };
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            var infoWindow = new google.maps.InfoWindow();
            var myLatlng = new google.maps.LatLng(40.7110411, -74.0110326);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
            (function (marker) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("" +
                            "<div class='map-properties contact-map-content'>" +
                            "<div class='map-content'>" +
                            "<p class='address'>123 Kathal St. Tampa City, </p>" +
                            "<ul class='map-properties-list'> " +
                            "<li><i class='fa fa-phone'></i>  +0477 8556 552</li> " +
                            "<li><i class='fa fa-envelope'></i>  info@themevessel.com</li> " +
                            "<li><a href='index.html'><i class='fa fa-globe'></i>  http://http://themevessel.com</li></a> " +
                            "</ul>" +
                            "</div>" +
                            "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker);
        }
        LoadMap();
    </script>
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>
</body>
</html>