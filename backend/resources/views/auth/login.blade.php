<!DOCTYPE html>
<html lang="en">
<head>
    <title>Big Space OK | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="author" content="Psybo Technologies">
    <!-- External CSS libraries -->
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-submenu.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-select.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/leaflet.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/map.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/flaticon/font/flaticon.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('fonts/linearicons/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/jquery.mCustomScrollbar.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/dropzone.css')}}">
   <!-- Custom stylesheet -->
   <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{url('css/default.css')}}">
   <!-- Favicon icon -->
   <link rel="shortcut icon" href="{{url('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{url('css/ie10-viewport-bug-workaround.css')}}">
     <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
     <script type="text/javascript" src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
</head>

<body>
    <div class="page_loader"></div>

    <!-- Content area start -->
    <div class="content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Main title -->
                            <div class="main-title">
                                <h1><span>Login</span></h1>
                            </div>
                             @if (count($errors) > 0)
                                <div layout="row" style="color: red;">
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <p>{{$error}} </p>
                                        @endforeach
                                    </div>
                                </div>
                             @endif
                            <!-- Form start -->
                            <form name="loginForm" method="post" action="{{URL::route('login')}}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input type="email" name="email" class="input-text" placeholder="Email Address">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="input-text" placeholder="Password">
                                </div>
                                <div class="checkbox">
                                    <div class="ez-checkbox pull-left">
                                        <label>
                                            <input type="checkbox" class="ez-hide">
                                            Remember me
                                        </label>
                                    </div>
                                    <a href="forgot-password.html" class="link-not-important pull-right">Forgot Password</a>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="button-md button-theme btn-block">login</button>
                                </div>
                            </form>
                            <!-- Form end -->
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>
                                New to User? <a href="signup.html">Sign up now</a>
                            </span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Content area end -->

    <script type="text/javascript" src="{{url('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{url('js/rangeslider.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mb.YTPlayer.js')}}"></script>
    <script type="text/javascript" src="{{url('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{url('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{url('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <!-- Custom javascript -->
    <script type="text/javascript" src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
         ga('create', 'UA-100829780-3', 'auto');
         ga('send', 'pageview');
    </script>
</body>
</html>