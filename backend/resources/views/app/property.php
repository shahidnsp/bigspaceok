<style>
    .container {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%)
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: #4CAF50;
        color: white;
        font-size: 16px;
        padding: 16px 32px;
    }
</style>
<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Property <small>Property Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Property
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.property.write=='true'" ng-click="newProperty();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Property</button>
            <form class="form-horizontal" ng-show="propertyedit" ng-submit="addProperty();">
                <h3>New Property </h3><br>
                <div class="row">
                    <div class="row col-sm-6">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.name" placeholder="Property Name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Location</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.location" placeholder="Property Location" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.price" placeholder="Property Price" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-10">
                                <select name="status" id="" class="form-control" ng-model="newproperty.status" placeholder="Property Status" required>
                                    <option value="For Rent">For Rent</option>
                                    <option value="For Sale">For Sale</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Area</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.area" placeholder="Property Area" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" ng-model="newproperty.address" placeholder="Address" required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">City</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.city" placeholder="Property City" >
                            </div>
                        </div>
                    </div>
                    <div class="row col-sm-6">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">District</label>
                            <div class="col-sm-10">
                                <!--<input type="text" class="form-control" ng-model="newproperty.district" placeholder="Property Distict">-->
                                <select class="form-control" name="district_id" ng-model="newproperty.district_id" required="">
                                    <option value="">Select State First</option>
                                    <option ng-repeat="district in districts" value="{{district.id}}">{{district.name}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">State</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="state_id" ng-model="newproperty.state_id" ng-change="getDistrict(newproperty.state_id)" required="">
                                    <option value=""></option>
                                    <option ng-repeat="state in states" value="{{state.id}}">{{state.name}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Pincode</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.pincode" placeholder="Property Pincode">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" ng-model="newproperty.description" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Contact name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.contactname" placeholder="Contact  Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.email" placeholder="Property Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" ng-model="newproperty.phone" placeholder="Property Phone" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Upload Images</label>
                        <div class="col-sm-10">
                            <input accept="image/*" ng-file-model="newproperty.photo" type="file" multiple/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div ng-repeat="photo in newproperty.photos" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                        <div class="container">
                            <img src="images/{{photo.name}}" alt="Avatar" class="image" style="width: 150px;height: 120px;">
                            <div class="middle">
                                <div class="text"><button type="button" ng-click="deletePhoto(photo);" class="btn btn-sm btn-danger">Delete</button></div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-11 text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" ng-click="cancelProperty();">Cancel</button>
                        </div>
                    </div>
                </div>

                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success" >Property  List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover" style="border-bottom: 8px solid #448aff;">
                    <thead>
                    <tr class="bg-primary">
                         <th>$</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Area</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Contact name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="property in listCount  = (propertys | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="viewProperty(property);">{{property.name}}</a></td>
                        <td>{{property.location}}</td>
                        <td>{{property.price}}</td>
                        <td>{{property.status}}</td>
                        <td>{{property.area}}</td>
                        <td>{{property.address}}</td>
                        <td>{{property.city}}</td>
                        <td>{{property.contactname}}</td>
                        <td>{{property.email}}</td>
                        <td>{{property.phone}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.property.edit=='true'" type="button" class="btn btn-primary" ng-click="editProperty(property);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </div>
                        </td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.property.delete=='true'" type="button" class="btn btn-danger" ng-click="deleteProperty(property);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="propertys.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>