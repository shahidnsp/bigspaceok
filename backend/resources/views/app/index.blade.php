<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Big Space Ok- Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/app.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body ng-controller="HomeController">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Administrator</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a ng-init="name='{{\Illuminate\Support\Facades\Auth::user()->name}}'"  class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> @{{name}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a ng-init="email='{{\Illuminate\Support\Facades\Auth::user()->email}}'" href="#profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="user/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li ng-repeat="menu in user.menu" ng-class="menu.name=='Dashboard' ? menuClass('') : menuClass('@{{menu.route}}')" ng-if="menu.type=='Main'">
                        <a href="#@{{menu.route}}"><i class="fa fa-fw @{{menu.icon}}"></i> @{{menu.name}}</a>
                    </li>

                     <li>
                         <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Basic Settings <i class="fa fa-fw fa-caret-down"></i></a>
                         <ul id="demo" class="collapse">
                             <li ng-repeat="menu in user.menu" ng-class="menuClass('@{{menu.route}}')" ng-if="menu.type=='Sub'">
                                 <a href="#@{{menu.route}}">@{{menu.name}}</a>
                             </li>
                             <!--<li ng-class="menuClass('propertyfeature')">
                                 <a href="#propertyfeature">Property Features</a>
                             </li>-->
                             <li ng-class="menuClass('profile')">
                                  <a href="#profile">My Profile</a>
                              </li>
                         </ul>
                     </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="row" ng-view></div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!--Angular Files-->
   <script src="js/angularjs.min.js"></script>
   <script src="js/myapp.js"></script>

</body>

</html>
