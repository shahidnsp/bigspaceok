<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<style>
    .ta-editor {
        min-height: 300px;
        height: auto;
        overflow: auto;
        font-family: inherit;
        font-size: 100%;
        margin: 20px 0;
    }
    .container {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%)
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: #4CAF50;
        color: white;
        font-size: 16px;
        padding: 16px 32px;
    }
</style>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            LatestDeal <small>LatestDeal Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Latest deal
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.latestdeal.write=='true'" ng-click="newLatestDeal();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Latest Deal</button>
            <form class="form-horizontal" ng-show="latestDealedit" ng-submit="addLatestDeal();">
                <h3>New Latest Deal </h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newlatestDeal.name" placeholder="Property Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Place</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newlatestDeal.place" placeholder="Place" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <div text-angular="text-angular" name="description" ng-model="newlatestDeal.description"></div>
                        <!--<text-angular ng-model="newlatestDeal.description"></text-angular>-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                        <input accept="image/*" ng-file-model="newlatestDeal.photo" type="file" multiple/>
                    </div>
                </div>

                <div class="row">
                    <div ng-repeat="photo in newlatestDeal.photos" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                        <div class="container">
                            <img src="images/{{photo.name}}" alt="Avatar" class="image" style="width: 150px;">
                            <div class="middle">
                                <div class="text"><button type="button" ng-click="deletePhoto(photo);" class="btn btn-sm btn-danger">Delete</button></div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelLatestDeal();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Latest Deal  List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover" style="border-bottom: 8px solid #448aff;">
                    <thead>
                    <tr class="bg-primary">
                        <th >$</th>
                        <th>Name</th>
                        <th>Place</th>
                        <th>Photo</th>
                        <th>Description</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="latestDeal in listCount  = (latestDeals | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{latestDeal.name}}</td>
                        <td>{{latestDeal.place}}</td>
                        <td><img src="images/{{latestDeal.photos[0].name}}" style="width: 120px;height: 120px;" alt=""/></td>
                        <td><p ng-bind-html="latestDeal.description | unsafe" class="description" popover="{{latestDeal.description}}" popover-trigger="mouseenter"></p></td>
                        <td>{{latestDeal.created_at}}</td>
                        <td>{{latestDeal.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.latestdeal.edit=='true'" type="button" class="btn btn-primary" ng-click="editLatestDeal(latestDeal);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </div>
                        </td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.latestdeal.delete=='true'" type="button" class="btn btn-danger" ng-click="deleteLatestDeal(latestDeal);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="latestDeals.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>