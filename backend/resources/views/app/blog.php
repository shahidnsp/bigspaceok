<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<style>
    .ta-editor {
        min-height: 300px;
        height: auto;
        overflow: auto;
        font-family: inherit;
        font-size: 100%;
        margin: 20px 0;
    }
</style>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Blog <small>Blog Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Blog
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.blog.write=='true'" ng-click="newBlog();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Blog</button>
            <form class="form-horizontal" ng-show="blogedit" ng-submit="addBlog();">
                <h3>New Blog </h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newblog.name" placeholder="Blog Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <div text-angular="text-angular" name="description" ng-model="newblog.description"></div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                        <input accept="image/*" ng-file-model="newblog.photo" type="file" multiple/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelBlog();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Blog  List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover" style="border-bottom: 8px solid #448aff;">
                    <thead>
                    <tr class="bg-primary">
                        <th>$</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Photo</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="blog in listCount  = (blogs | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                            <td>{{blog.name}}
                            <td><p class="description" ng-bind-html="blog.description | unsafe"  popover="{{blog.description | unsafe}}" popover-trigger="mouseenter"></p></td>

                            <td><img src="images/{{blog.photo}}" style="height: 120px;width: 120px;" alt=""/></td>
                            <td>{{blog.created_at}}</td>
                            <td>{{blog.updated_at}}</td>
                            <td>
                                <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button ng-if="user.permissions.blog.edit=='true'" type="button" class="btn btn-primary btn-xs" ng-click="editBlog(blog);">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </div>
                            </td>
                            <td>
                                <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button ng-if="user.permissions.blog.delete=='true'" type="button" class="btn btn-danger btn-xs" ng-click="deleteBlog(blog);">
                                        <i class="fa  fa-trash-o"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="blogs.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>