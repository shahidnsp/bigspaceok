<!--<button type="button" class="close" ng-click="cancel();" >-->
<!--    <i class="fa fa-times-circle-o" style="margin:10px;color:#ff0000;"></i>-->
<!--</button>-->
<span class="pull-right">
<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i  class="fa fa-close"  ></i></a>
</span>

<div  class="modal-header  bg-primary" style="padding:8px" >

    <h3 class="modal-title">Name: {{property.name}}</h3>
</div>
<div class="modal-body bg-warning">
    <!--<div class="container">-->
    <div class="row">
        <div class="panel panel-info">
            <!--<div class="panel-heading">{{property.name}}</div>-->
            <div class="panel-body">
                <div class="box box-info">
                    <div class="box-body">
                        <div class="col-sm-6">

                            <h4 >{{property.name}}</h4>
                        </div>
                        <div class="clearfix"></div>
                        <hr style="margin:5px 0 5px 0;">
                        <div class="col-sm-3 col-xs-6 title "  >Location:</div><div class="col-sm-7 col-xs-6 ">{{property.location}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Price:</div><div class="col-sm-7">  {{property.price}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Status:</div><div class="col-sm-7"> {{property.status}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Area:</div><div class="col-sm-7">{{property.area}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Address:</div><div class="col-sm-7"> {{property.address}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>

                        <div class="col-sm-3 col-xs-6 title " >City:</div><div class="col-sm-7">{{property.city}}</div>


                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >District:</div><div class="col-sm-7">{{property.district.name}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Sate:</div><div class="col-sm-7">{{property.state.name}}</div>


                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >PIN:</div><div class="col-sm-7">{{property.pincode}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Description:</div><div class="col-sm-7">{{property.description}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Contact name:</div><div class="col-sm-7">{{property.contactname}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Email:</div><div class="col-sm-7">{{property.email}}</div>


                        <div class="clearfix"></div>
                        <div class="bot-border"></div>

                        <div class="col-sm-3 col-xs-6 title " >Phone:</div><div class="col-sm-7">{{property.phone}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Created_at:</div><div class="col-sm-7">{{property.created_at}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                        <div class="col-sm-3 col-xs-6 title " >Updated_at:</div><div class="col-sm-7">{{property.updated_at}}</div>

                        <div class="clearfix"></div>
                        <div class="bot-border"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" ng-click="cancel();">Close</button>
        </div>
    </div>
<!--</div>-->

</div>
