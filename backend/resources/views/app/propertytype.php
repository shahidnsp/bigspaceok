<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            PropertyType <small>PropertyType Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Property Types
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.propertytype.write=='true'" ng-click="newPropertyType();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Property Type</button>
            <form class="form-horizontal" ng-show="propertyTypeedit" ng-submit="addPropertyType();">
                <h3>New Property Type</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newpropertyType.name" placeholder="PropertyType Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="newpropertyType.description" placeholder="Description" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelPropertyType();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">PropertyType List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover" style="border-bottom: 8px solid #448aff;">
                    <thead>
                    <tr class="bg-primary">
                        <th>$</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="propertyType in listCount  = (propertyTypes | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{propertyType.name}}</td>
                        <td><p class="description" popover="{{propertyType.description}}" popover-trigger="mouseenter">{{propertyType.description}}</p></td>
                        <td>{{propertyType.created_at}}</td>
                        <td>{{propertyType.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.propertytype.edit=='true'" type="button" class="btn btn-primary btn-xs" ng-click="editPropertyType(propertyType);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </div>
                        </td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button ng-if="user.permissions.propertytype.delete=='true'" type="button" class="btn btn-danger btn-xs" ng-click="deletePropertyType(propertyType);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="propertyTypes.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>