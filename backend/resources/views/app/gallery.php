<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<style>
    .container {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%)
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: #4CAF50;
        color: white;
        font-size: 16px;
        padding: 16px 32px;
    }
</style>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Testimonial <small>Testimonial Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Gallery
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.gallery.write=='true'" ng-click="newGallery();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Gallery</button>
            <form class="form-horizontal" ng-show="galleryedit" ng-submit="addGallery();">
                <h3>New Gallery </h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                        <input accept="image/*" ng-file-model="newgallery.photo" type="file" multiple/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelGallery();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Gallery  List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <div class="row">
                    <div ng-repeat="gallery in listCount  = (galleries | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                        <div class="container">
                            <img src="images/{{gallery.photo}}" alt="Avatar" class="image" style="width: 150px;height: 120px;">
                            <div class="middle">
                                <div class="text"><button ng-if="user.permissions.gallery.delete=='true'" ng-click="deletePhoto(gallery);" class="btn btn-sm btn-danger">Delete</button></div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
               <!-- <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Name</th>
                        <th>Content</th>
                        <th>Photo</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="testimonial in listCount  = (testimonials | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{testimonial.name}}</td>
                        <td>{{testimonial.content}}</td>
                        <td><img src="images/{{testimonial.photo}}" style="height: 120px;width: 120px;" alt=""/></td>
                        <td>{{testimonial.created_at}}</td>
                        <td>{{testimonial.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editTestimonial(testimonial);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteTestimonial(testimonial);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="galleries.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>