<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Change Profile
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">

            <tabset>
                <tab heading="Profile" active="true">
                    <br/><br/>
                    <form class="form-horizontal" ng-submit="updateProfile();">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Profile Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="name" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="email" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 text-center">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                </tab>
                <tab heading="Change Password">
                    <br/>
                    <form class="form-horizontal" ng-submit="changePassword(curpassword,newpassword,repassword);">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Current Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="curpassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="newpassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Re-enter Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="repassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 text-center">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                </tab>
                <tab ng-show="user.isAdmin=='1'" heading="Users">
                    <br/>
                    <button ng-click="newUser();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add User</button>
                    <div ng-show="useredit" class="row">
                        <form class="form-horizontal" ng-submit="addUser();">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">User Type</label>
                                <div class="col-sm-5">
                                    <select class="form-control" ng-model="newuser.isAdmin" id="">
                                        <option value="1">Admin</option>
                                        <option value="0">User</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.name" required=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" ng-model="newuser.email" required=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.phone"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-5">
                                    <textarea class="form-control" ng-model="newuser.address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">City</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.city"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">District</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.district"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">State</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.state"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Pin</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" ng-model="newuser.pin"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" ng-click="cancelUser();">Cancel</button>
                                </div>
                            </div>
                            <hr>
                        </form>
                    </div>

                    <div ng-hide="resetPermission" class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-success">Users  List</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="expenseTable" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>$</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Password</th>
                                            <th>Permission</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="user in listCount  = (users | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                            <td>{{user.name}}</td>
                                            <td>{{user.email}}</td>
                                            <td>{{user.phone}}</td>
                                            <td><p  class="description" popover="{{user.address}}" popover-trigger="mouseenter">{{user.address}}</p></td>
                                            <td style="text-align: center;">
                                                <button ng-click="resetPassword(user);" type="button" class="btn btn-danger">
                                                    Reset
                                                </button>
                                            </td>
                                            <td style="text-align: center;">
                                                <button ng-click="permissionMode(user);" type="button" class="btn btn-primary">
                                                    <i class="fa fa-key"></i>
                                                </button>
                                            </td>
                                            <td>
                                                <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                    <button type="button" class="btn btn-primary" ng-click="editUser(user);">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-default" ng-click="deleteUser(user);">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix" ng-show="users.length > numPerPage">
                            <pagination
                                ng-model="currentPage"
                                total-items="listCount.length"
                                max-size="maxSize"
                                items-per-page="numPerPage"
                                boundary-links="true"
                                class="pagination-sm pull-right"
                                previous-text="&lsaquo;"
                                next-text="&rsaquo;"
                                first-text="&laquo;"
                                last-text="&raquo;"
                                ></pagination>
                        </div>

                    </div>

                    <div ng-show="resetPermission">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="javascript:void(0)" ng-click="backToUsers($event);"><i class="fa fa-home"></i></a>
                                            <span ng-click="backToUsers($event);">User:{{name}}</span>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" ><i class="fa fa-key"></i></a>
                                            <span>Permission</span>
                                        </li>
                                    </ol>
                                </div>
                                <div>
                                    Select the pages you want add for this user from available pages and then you can add write,edit (White color is for not allowed) permission for those pages
                                </div>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item active">
                                        Permited pages
                                        <md-button class="md-raised md-small  md-primary pull-right" ng-click="savePermission($event)">Save</md-button>
                                    </li>
                                    <li class="list-group-item">
                                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterMypermission" />
                                    </li>
                                    <li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission">
                                        {{mypermision.name}} <a href ng-click="removeToList(mypermision);"><i class="fa fa-arrow-right"></i></a>

                                        <div class="btn-group btn-group-xs pull-right">
                                            <label class="btn" ng-model="mypermision.write" ng-click="toggleWrite(mypermision)"  ng-class="mypermision.write === 'true'? 'btn-primary' : 'btn-default'">
                                                write
                                            </label>&nbsp;
                                            <label class="btn" ng-model="mypermision.edit" ng-click="toggleEdit(mypermision)"  ng-class="mypermision.edit === 'true'? 'btn-success' : 'btn-default'">
                                                Edit
                                            </label>&nbsp;
                                            <label class="btn" ng-model="mypermision.delete" ng-click="toggleDelete(mypermision)"  ng-class="mypermision.delete === 'true'? 'btn-danger' : 'btn-default'">
                                                Delete
                                            </label>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item active">Available pages</li>
                                    <li class="list-group-item">
                                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterPage" />
                                    </li>
                                    <li class="list-group-item" ng-repeat="page in pages | filter:filterPage">
                                        <a href ng-click="addToList(page);"><i class="fa fa-arrow-left"></i></a> {{page.name}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row text-center">
                                    <md-button ng-click="savePermission($event)" class="md-raised  md-primary text-center" > <md-icon>note_add</md-icon>Update Permission</md-button>
                                </div>
                            </div>
                        </div>
                    </div>

                </tab>
            </tabset>


        </div>
    </div>
</div>

<br/>
