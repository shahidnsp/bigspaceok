<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureList extends Model
{
    protected $fillable=['property_id','property_feature_id'];
}
