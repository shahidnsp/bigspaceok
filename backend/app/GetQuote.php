<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GetQuote extends Model
{
    protected $fillable=['name','email','number1','number2','place','property_id'];
}
