<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable=['name','description','photo'];

    public function next(){
        // get next user
        return Blog::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  user
        return Blog::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }
}
