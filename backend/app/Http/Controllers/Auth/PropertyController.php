<?php

namespace App\Http\Controllers\Auth;

use App\Photo;
use App\Property;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;
use Response;
use Image;
use Storage;

class PropertyController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'status'=>'required',
            'area'=>'required',
            'address'=>'required',
            'contactname'=>'required',
            'phone'=>'required',
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $isSale=$request->isSale;
        if($isSale==null)
            return Property::with('photos')->with('state')->with('district')->get();
        else
            return Property::with('photos')->where('isSale',$isSale)->get();
    }

    public function search(Request $request){
        $state_id=$request->state_id;
        $district_id=$request->district_id;

        $properties=Property::with('photos')->where('district_id',$district_id)->paginate(8);
        return view('frontend.search',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $property=new Property($request->all());

        if($property->save()) {
            $photos=$request->photo;
            if($photos!=null){
                foreach($photos as $photo){
                    $photonew=new Photo();
                    $photonew->name= $this->savePhoto($photo['data']);
                    $photonew->property_id=$property->id;
                    $photonew->save();
                }
            }
            return $property;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'property'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(848, 385)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $property=Property::findOrfail($id);
        $property->fill($request->all());


        if($property->update()) {
            $photos=$request->photo;
            if($photos!=null){
                foreach($photos as $photo){
                    $photonew=new Photo();
                    $photonew->name= $this->savePhoto($photo['data']);
                    $photonew->property_id=$property->id;
                    $photonew->save();
                }
            }
            return $property;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curphotos=Photo::where('property_id',$id)->get();
        foreach($curphotos as $curphoto){
            $exist = Storage::disk('local')->exists($curphoto->name);
            if ($exist)
                Storage::delete($curphoto->name);
            $curphoto->delete();
        }
        if(Property::destroy($id)) {
            return Response::json(array('msg' => 'Property record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    
    }

    public function submitProperty(Request $request){
        $validator = $this->validator($request->all());
        if($validator->fails()){
            \Session::flush();
            \Session::flash('error', "Ohps...Try Again");
            return redirect()->back();
        }

        $property=new Property($request->all());

        if($property->save()) {

            $array=Session::get('imageArray');
            foreach($array as $arr){
                $photo=new Photo();
                $photo->name=$arr;
                $photo->property_id=$property->id;
                $photo->save();
            }

            \Session::flush();
            \Session::flash('message', "Thank you...We Contact you shortly ");
            return redirect()->back();
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }
}
