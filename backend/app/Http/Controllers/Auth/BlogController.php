<?php

namespace App\Http\Controllers\Auth;

use App\Blog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Storage;

class BlogController extends Controller
{
    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'description'=>'required',
            //'photo'=>'required',
        ]);
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Blog::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $blog=new Blog($request->all());
        $photos=$request->photo;
        if($photos!=null){
            foreach($photos as $photo){
                $blog->photo= $this->savePhoto($photo['data']);
            }
        }else{
            $blog->photo='dummydeals.jpg';
        }
        if($blog->save()) {
            return $blog;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'blog'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(848, 385)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog=\App\Blog::find($id);
        if($blog)
            return view('frontend.single-blog',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $blog=Blog::findOrfail($id);
        $blog->fill($request->all());
        $photos=$request->photo;
        if($photos!=null){
            foreach($photos as $photo){
                $blog->photo= $this->savePhoto($photo['data']);
            }
        }

        if($blog->update()) {
            return $blog;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog=Blog::find($id);
        if($blog){
            if (strcmp($blog->photo,'dummydeals.jpg')==1) {
                $exist = Storage::disk('local')->exists($blog->photo);
                if ($exist)
                    Storage::delete($blog->photo);
            }
        }

        if(Blog::destroy($id)) {
            return Response::json(array('msg' => 'Blog record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    
    }
}
