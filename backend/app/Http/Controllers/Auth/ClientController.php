<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Storage;

class ClientController extends Controller
{
    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'logo'=>'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }
        $photos=$request->logo;
        if($photos!=null){
            $client=new Client($request->all());
            foreach($photos as $photo){
                $client->logo= $this->savePhoto($photo['data']);
            }
            if($client->save()) {
                return $client;
            }
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'client'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(222, 82)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $client=Client::findOrfail($id);
        $client->fill($request->all());
        $photos=$request->logo;
        if($photos!=null){
            foreach($photos as $photo){
                $client->logo= $this->savePhoto($photo['data']);
            }
        }

        if($client->update()) {
            return $client;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client=Client::find($id);
        if($client){
            if($client->logo!='logo.png') {
                $exist = Storage::disk('local')->exists($client->logo);
                if ($exist)
                    Storage::delete($client->logo);
            }
        }

        if(Client::destroy($id)) {
            return Response::json(array('msg' => 'Client record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}
