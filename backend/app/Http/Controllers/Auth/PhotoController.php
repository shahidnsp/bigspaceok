<?php

namespace App\Http\Controllers\Auth;

use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;
use Response;
use Storage;
use Image;


class PhotoController extends Controller
{
    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'property_id'=>'required',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Photo::all();

    }

    public function fileUpload(Request $request){
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $filename= $this->uploadImage($file);

            $array=[];
            $array=Session::get('imageArray');
            if($array==null){
                $array[0]=$filename;
                Session::put('imageArray',$array);
            }else{
                array_push($array,$filename);
                Session::put('imageArray',$array);
            }
            return $array;
        }
    }

    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = 'property'.rand(11111,99999).'.'.$extension;
            $imageRealPath 	= 	$file->getRealPath();
            $img = Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval('500'), null, function($constraint) {
                $constraint->aspectRatio();
            });
            $storedFileName=$fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            //$img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;*/
            $image = Image::make($file->getRealPath())->resize(848, 385);//->save($path);

            Storage::disk('local')->put($fileName,$image->stream());//file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $photo=new Photo($request->all());

        if($photo->save()) {
            return $photo;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $photo=Photo::findOrfail($id);
        $photo->fill($request->all());


        if($photo->update()) {
            return $photo;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo=Photo::find($id);
        if($photo){
            if (strcmp($photo->name,'dummydeals.jpg')==1) {
                $exist = Storage::disk('local')->exists($photo->name);
                if ($exist)
                    Storage::delete($photo->name);
            }
            if (strcmp($photo->temp,'temp.jpg')==1) {
                $exist = Storage::disk('local')->exists($photo->temp);
                if ($exist)
                    Storage::delete($photo->temp);
            }
        }

        if(Photo::destroy($id)) {
            return Response::json(array('msg' => 'Photo record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    
    }
}
