<?php

namespace App\Http\Controllers\Auth;

use App\GetQuote;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class GetQuoteController extends Controller
{
    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'number1'=>'required',
            'place'=>'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GetQuote::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $quote=new GetQuote($request->all());

        if($quote->save()) {
            return $quote;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    public function getQuotes(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $quote=new GetQuote($request->all());

        if($quote->save()) {
            \Session::flush();
            \Session::flash('message', "Thank you...We Contact you shortly ");
            return redirect()->back();
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $quote=GetQuote::findOrfail($id);
        $quote->fill($request->all());


        if($quote->update()) {
            return $quote;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(GetQuote::destroy($id)) {
            return Response::json(array('msg' => 'FeatureList record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}
