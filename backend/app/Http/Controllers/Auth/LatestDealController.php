<?php

namespace App\Http\Controllers\Auth;

use App\LatestDeal;
use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Storage;

class LatestDealController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'place'=>'required',
            'description'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LatestDeal::with('photos')->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $latestdeal=new LatestDeal($request->all());
        $photos=$request->photo;

        if($latestdeal->save()) {
            if($photos!=null){
                foreach($photos as $photo){
                    $photonew=new Photo();
                    $photonew->name= $this->savePhoto($photo['data']);
                    $photonew->temp= $this->savePhotoTemp($photo['data']);
                    $photonew->latest_deal_id=$latestdeal->id;
                    $photonew->save();
                }
            }
            return LatestDeal::with('photos')->find($latestdeal->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'deal'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(850, 592)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    private function savePhotoTemp($photo)
    {
        $fileName = '';
        try {
            if (strlen($photo) > 128) {
                list($ext, $data) = explode(';', $photo);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';') - 11);
                $fileName = 'dealtemp' . rand(11111, 99999) . '.' . $mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img = Image::make($data)->resize(200, 150)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName, $img);

            }
        } catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }


        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project=LatestDeal::with('photos')->where('id',$id)->get()->first();
        if($project)
            return view('frontend.single-project',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $latestdeal=LatestDeal::findOrfail($id);
        $latestdeal->fill($request->all());


        if($latestdeal->update()) {
            $photos=$request->photo;
            if($photos!=null) {
                foreach($photos as $photo){
                    $photonew=new Photo();
                    $photonew->name= $this->savePhoto($photo['data']);
                    $photonew->temp= $this->savePhotoTemp($photo['data']);
                    $photonew->latest_deal_id=$latestdeal->id;
                    $photonew->save();
                }
            }
            return LatestDeal::with('photos')->find($latestdeal->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photos=Photo::where('latest_deal_id',$id)->get();
        if($photos) {
            foreach ($photos as $photo) {
                if (strcmp($photo->name,'dummydeals.jpg')==1) {
                    $exist = Storage::disk('local')->exists($photo->name);
                    if ($exist)
                        Storage::delete($photo->name);
                }

               if (strcmp($photo->temp,'temp.jpg')==1) {
                    $exist = Storage::disk('local')->exists($photo->temp);
                    if ($exist)
                        Storage::delete($photo->temp);
                }
                $photo->delete();
            }



        }

        if(LatestDeal::destroy($id)) {
            return Response::json(array('msg' => 'LatestDeal record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    
    }
}
