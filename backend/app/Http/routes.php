<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    //return view('welcome');
    if(\Illuminate\Support\Facades\Auth::check()) {
        if(\Illuminate\Support\Facades\Auth::user()->usertype=='Admin')
            return view('app.index');
        else
            return view('frontend.index');
    } else {
        return view('frontend.index');
    }

}]);

// Loggedin page
Route::group(['prefix' => 'user'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});

Route::get('/sales', function () {
    $properties=\App\Property::with('photos')->where('isSale',1)->where('approve',1)->where('status','For Sale')->paginate(3);
    return view('frontend.sales',compact('properties'));
});

Route::get('/rentals', function () {
    $properties=\App\Property::with('photos')->where('isSale',1)->where('approve',1)->where('status','For Rent')->paginate(3);
    return view('frontend.rentals',compact('properties'));
});

Route::get('/projects', function () {
    $projects=\App\LatestDeal::paginate(4);
    return view('frontend.projects',compact('projects'));
});

Route::get('/blogs', function () {
    $blogs=\App\Blog::paginate(6);
    return view('frontend.blogs',compact('blogs'));
});

Route::get('/moments', function () {
    $galleries=\App\Gallery::all();
    return view('frontend.gallery',compact('galleries'));
});

Route::get('/contact', function () {
    return view('frontend.contact');
});

Route::get('/submit-property', function () {
    $states=\App\State::all();
    return view('frontend.submit-property',compact('states'));
});

Route::get('/test',  function () {
    $input = array("a", "b", "c", "d", "e");
    return $output = array_slice($input, 0, 7);
});


Route::get('images/{filename}', function ($filename)
{
    $file = \Illuminate\Support\Facades\Storage::get($filename);
    return response($file, 200)->header('Content-Type', 'image/jpeg');
    //return base64_encode($file);
});

Route::group(['middleware'=>'auth'],function() {
// Loggedin page
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Auth\DashboardController@index']);
});

//TODO add middleware to authenticate
Route::group(['middleware'=>'auth','prefix'=>'api'],function() {
//Route::group(['prefix' => 'api'], function () {
    //Rest resources
    Route::resource('client', 'Auth\ClientController');
    Route::resource('featurelist', 'Auth\FeatureListController');
    Route::resource('latestdeal', 'Auth\LatestDealController');
    Route::resource('photo', 'Auth\PhotoController');
    Route::resource('property', 'Auth\PropertyController');
    Route::resource('propertyfeature', 'Auth\PropertyFeatureController');
    Route::resource('propertytype', 'Auth\PropertyTypeController');
    Route::resource('testimonial', 'Auth\TestimonialController');
    Route::resource('blog', 'Auth\BlogController');
    Route::resource('contact', 'Auth\ContactController');
    Route::resource('featuredproperty', 'Auth\FeaturedPropertyController');
    Route::resource('quote', 'Auth\GetQuoteController');
    Route::resource('gallery', 'Auth\GalleryController');
    Route::resource('moment', 'Auth\MomentController');
    Route::resource('user', 'Auth\UserController');


    Route::get('userinfo',['as'=>'userinfo','uses'=>'Auth\UserInfoController@UserInfoController']);
    Route::get('getAllPages/{id}',['as'=>'getAllPages','uses'=>'Auth\UserInfoController@getAllPages']);
    Route::get('getPermission',['as'=>'getPermission','uses'=>'Auth\UserInfoController@getUserPermission']);
    Route::post('changePermission',['as'=>'setPermission','uses'=>'Auth\UserInfoController@setUserPermission']);

    Route::post('updateprofile', 'Auth\UserController@updateMyProfile');
    Route::post('changepassword', 'Auth\UserInfoController@changeUserPassword');

});


Route::post('getQuotes','Auth\GetQuoteController@getQuotes');
Route::post('getintouch','Auth\ContactController@getInTouch');
Route::post('file-upload','Auth\PhotoController@fileUpload');
Route::get('single-blog/{id}','Auth\BlogController@show');
Route::get('single-project/{id}','Auth\LatestDealController@show');
Route::post('submit-property', 'Auth\PropertyController@submitProperty');
Route::get('getDistricts', 'Auth\DistrictController@index');
Route::get('getState', 'Auth\StateController@index');

Route::get('search', 'Auth\PropertyController@search');



//Load angular templates
Route::group(['middleware'=>'auth'],function() {
    Route::get('template/{name}', ['as' => 'templates', function ($name) {
        return view('app.' . $name);
    }]);
});


// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('errors.503');
    //return redirect('/');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');
