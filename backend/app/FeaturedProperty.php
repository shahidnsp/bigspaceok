<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedProperty extends Model
{
    protected $fillable=['property_id','description','order_id',];

    public function property(){
        return $this->belongsTo('App\Property');
    }
}
