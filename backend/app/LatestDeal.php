<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestDeal extends Model
{
    protected $fillable=['name','place','description'];

    public function photos(){
        return $this->hasMany('App\Photo','latest_deal_id');
    }

    public function next(){
        // get next user
        return LatestDeal::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  user
        return LatestDeal::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }
}
