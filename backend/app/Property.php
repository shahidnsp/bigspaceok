<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable=['name','location','price','status','monthorannual','area','address','city','district_id','state_id','pincode','description','contactname','email','phone','approve','isSale','latest_deals_id','property_type_id','user_id'];

    public function photos(){
        return $this->hasMany('App\Photo','property_id');
    }

    public function featured_properties(){
        return $this->hasMany('App\Photo','property_id');
    }

    public function state(){
        return $this->belongsTo('App\State','state_id');
    }

    public function district(){
        return $this->belongsTo('App\District','district_id');
    }

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();

        static::deleting(function($property) { // before delete() method call this
            $property->photos()->delete();
            // do the rest of the cleanup...
        });
    }
}
