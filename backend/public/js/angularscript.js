var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'textAngular',
        'PropertyTypeService',
        'PropertyFeatureService',
        'ClientService',
        'TestimonialService',
        'LatestDealService',
        'PropertyService',
        'BlogService',
        'ContactService',
        'FeaturedPropertyService',
        'GalleryService',
        'EnquiryService',
        'UserService',
        'UserInfoService',
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/propertytype', {
            templateUrl: 'template/propertytype',
            controller: 'PropertyTypeController'
        })
        .when('/propertyfeature', {
            templateUrl: 'template/propertyfeature',
            controller: 'PropertyFeatureController'
        })
        .when('/client', {
            templateUrl: 'template/client',
            controller: 'ClientController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/latestdeal', {
            templateUrl: 'template/latestdeal',
            controller: 'LatestDealController'
        })
        .when('/property', {
                templateUrl: 'template/property',
                controller: 'PropertyController'
        })
        .when('/enquiry', {
                templateUrl: 'template/enquiry',
                controller: 'EnquiryController'
        })
        .when('/blog', {
                templateUrl: 'template/blog',
                controller: 'BlogController'
          })
        .when('/contact', {
                templateUrl: 'template/contact',
                controller: 'ContactController'
            })
        .when('/featured_property', {
            templateUrl: 'template/featured_property',
            controller: 'FeaturedPropertyController'
        })
        .when('/gallery', {
            templateUrl: 'template/gallery',
            controller: 'GalleryController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'ProfileController'
        })

		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();

                        reader.onload = (function (i) {
                            return function(e) {
                                var value = {
                                    lastModified: changeEvent.target.files[i].lastModified,
                                    lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                    name: changeEvent.target.files[i].name,
                                    size: changeEvent.target.files[i].size,
                                    type: changeEvent.target.files[i].type,
                                    data: e.target.result
                                };
                                values.push(value);
                            }

                        })(i);

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }


                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });

    app.filter('unsafe', function ($sce) {
        return function (val) {
            if( (typeof val == 'string' || val instanceof String) ) {
                return $sce.trustAsHtml(val);
            }
        };
    });
