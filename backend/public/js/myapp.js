var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'textAngular',
        'PropertyTypeService',
        'PropertyFeatureService',
        'ClientService',
        'TestimonialService',
        'LatestDealService',
        'PropertyService',
        'BlogService',
        'ContactService',
        'FeaturedPropertyService',
        'GalleryService',
        'EnquiryService',
        'UserService',
        'UserInfoService',
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/propertytype', {
            templateUrl: 'template/propertytype',
            controller: 'PropertyTypeController'
        })
        .when('/propertyfeature', {
            templateUrl: 'template/propertyfeature',
            controller: 'PropertyFeatureController'
        })
        .when('/client', {
            templateUrl: 'template/client',
            controller: 'ClientController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/latestdeal', {
            templateUrl: 'template/latestdeal',
            controller: 'LatestDealController'
        })
        .when('/property', {
                templateUrl: 'template/property',
                controller: 'PropertyController'
        })
        .when('/enquiry', {
                templateUrl: 'template/enquiry',
                controller: 'EnquiryController'
        })
        .when('/blog', {
                templateUrl: 'template/blog',
                controller: 'BlogController'
          })
        .when('/contact', {
                templateUrl: 'template/contact',
                controller: 'ContactController'
            })
        .when('/featured_property', {
            templateUrl: 'template/featured_property',
            controller: 'FeaturedPropertyController'
        })
        .when('/gallery', {
            templateUrl: 'template/gallery',
            controller: 'GalleryController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'ProfileController'
        })

		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();

                        reader.onload = (function (i) {
                            return function(e) {
                                var value = {
                                    lastModified: changeEvent.target.files[i].lastModified,
                                    lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                    name: changeEvent.target.files[i].name,
                                    size: changeEvent.target.files[i].size,
                                    type: changeEvent.target.files[i].type,
                                    data: e.target.result
                                };
                                values.push(value);
                            }

                        })(i);

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }


                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });

    app.filter('unsafe', function ($sce) {
        return function (val) {
            if( (typeof val == 'string' || val instanceof String) ) {
                return $sce.trustAsHtml(val);
            }
        };
    });

/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('BlogService',[]).factory('Blog',['$resource',
    function($resource){
        return $resource('/api/blog/:blogId',{
            blogId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('ClientService',[]).factory('Client',['$resource',
    function($resource){
        return $resource('/api/client/:clientId',{
            clientId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/api/contact/:contactId',{
            contactId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('EnquiryService',[]).factory('Enquiry',['$resource',
    function($resource){
        return $resource('/api/quote/:quoteId',{
            quoteId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('FeaturedPropertyService',[]).factory('FeaturedProperty',['$resource',
    function($resource){
        return $resource('/api/featuredproperty/:featuredpropertyId',{
            featuredpropertyId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('GalleryService',[]).factory('Gallery',['$resource',
    function($resource){
        return $resource('/api/gallery/:galleryId',{
            galleryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('LatestDealService',[]).factory('LatestDeal',['$resource',
    function($resource){
        return $resource('/api/latestdeal/:latestdealId',{
            latestdealId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyFeatureService',[]).factory('PropertyFeature',['$resource',
    function($resource){
        return $resource('/api/propertyfeature/:propertyfeatureId',{
            propertyfeatureId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyService',[]).factory('Property',['$resource',
    function($resource){
        return $resource('/api/property/:propertyId',{
            propertyId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('PropertyTypeService',[]).factory('PropertyType',['$resource',
    function($resource){
        return $resource('/api/propertytype/:propertytypeId',{
            propertytypeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('TestimonialService',[]).factory('Testimonial',['$resource',
    function($resource){
        return $resource('/api/testimonial/:testimonialId',{
            testimonialId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 11/05/2017.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
    var UserInfo = {};

    UserInfo.query=function(){
        return $http.get('/api/userinfo')
    };


    UserInfo.get = function(id){
        return $http.get('/api/user/'+id);
    };

    UserInfo.save = function(id,data){
        return $http.put('/api/userinfo/'+id,data);
    };

    UserInfo.getAllPage=function(id){
        return $http.get('/api/getAllPages/'+id);
    };

    UserInfo.getUserPermission=function(userId){
        return $http.get('/api/getPermission/?id='+userId);
    };

    UserInfo.setUserPermission = function(data){
        return $http.post('/api/changePermission',data);
    };

    return UserInfo;
}
]);

/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/api/user/:userId',{
            userId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
app.controller('BlogController', function($scope,$http,$anchorScroll,ngNotify,Blog){
    $scope.blogs=[];
    $scope.blogedit=false;

    Blog.query(function(blog){
        $scope.blogs=blog;
    });

    $scope.newBlog = function (argument) {
        $scope.blogedit = true;
        $scope.newblog = new Blog();
        $scope.curBlog = {};
    };
    $scope.editBlog = function (thisBlog) {
        $scope.blogedit = true;
        $scope.curBlog =  thisBlog;
        $scope.newblog = angular.copy(thisBlog);
        $anchorScroll();
    };
    $scope.addBlog = function () {

        if ($scope.curBlog.id) {
            $scope.newblog.$update(function(blog){
                angular.extend($scope.curBlog, $scope.curBlog, blog);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Updated Successfully');
            });
        } else{
            $scope.newblog.$save(function(blog){
                $scope.blogs.push(blog);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Saved Successfully');
            });
        }
        $scope.blogedit = false;
        $scope.newblog = new Blog();
    };
    $scope.deleteBlog = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.blogs.indexOf(item);
                $scope.blogs.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Blog Removed Successfully');
            });
        }
    };
    $scope.cancelBlog = function () {
        $scope.blogedit = false;
        $scope.newblog = new Blog();
    };
});
app.controller('ClientController', function($scope,$http,$anchorScroll,ngNotify,Client){
    $scope.clients=[];
    $scope.clientedit=false;

    Client.query(function(client){
        $scope.clients=client;
    });

    $scope.newClient = function (argument) {
        $scope.clientedit = true;
        $scope.newclient = new Client();
        $scope.curClient = {};
    };
    $scope.editClient = function (thisClient) {
        $scope.clientedit = true;
        $scope.curClient =  thisClient;
        $scope.newclient = angular.copy(thisClient);
        $anchorScroll();
    };
    $scope.addClient = function () {

        if ($scope.curClient.id) {
            $scope.newclient.$update(function(client){
                angular.extend($scope.curClient, $scope.curClient, client);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Updated Successfully');
            });
        } else{
            $scope.newclient.$save(function(client){
                $scope.clients.push(client);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Saved Successfully');
            });
        }
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
    $scope.deleteClient = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.clients.indexOf(item);
                $scope.clients.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Client Removed Successfully');
            });
        }
    };
    $scope.cancelClient = function () {
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
});
app.controller('ContactController', function($scope,$http,$anchorScroll,ngNotify,Contact){
    $scope.contacts=[];
    $scope.contactedit=false;

    Contact.query(function(contact){
        $scope.contacts=contact;
    });

});
app.controller('DashboardController', function($scope,$http){

});
app.controller('EnquiryController', function($scope,$http,$anchorScroll,ngNotify,Enquiry,$modal){
    $scope.enquiries=[];
    $scope.enquiryedit=false;
    
    

    Enquiry.query(function(enquiry){
        $scope.enquiries=enquiry;
    });
    


    $scope.newEnquiry = function (argument) {
        $scope.enquiryedit = true;
        $scope.newenquiry = new Enquiry();
        $scope.curEnquiry = {};
    };
    $scope.editEnquiry = function (thisEnquiry) {
        $scope.enquiryedit = true;
        $scope.curEnquiry =  thisEnquiry;
        $scope.newenquiry = angular.copy(thisEnquiry);
        $anchorScroll();
    };

    $scope.cancelEnquiry = function () {
        $scope.enquiryedit = false;
        $scope.newenquiry = new Enquiry();
    };
    
    
    $scope.addEnquiry = function () {

        if ($scope.curEnquiry.id) {
            $scope.newenquiry.$update(function(enquiry){
                angular.extend($scope.curEnquiry, $scope.curEnquiry, enquiry);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Updated Successfully');
            });
        } else{
            $scope.newenquiry.$save(function(enquiry){
                $scope.enquiries.push(enquiry);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Saved Successfully');
            });
        }
        $scope.enquiryedit = false;
        $scope.newenquiry = new Enquiry();
    };


    $scope.deleteEnquiry = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.enquiries.indexOf(item);
                $scope.enquiries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Enquiry Removed Successfully');
            });
        }
    };

    $scope.viewEnquiry = function (p) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewenquiry',
            controller:'ViewEnquiryController',
            size: 'lg',
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    }; 
    
});



app.controller('ViewEnquiryController', function ($scope, $modalInstance,$modal, item) {

    $scope.enquiry = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

});
app.controller('FeaturedPropertyController', function($scope,$http,$anchorScroll,ngNotify,FeaturedProperty,Property){
    $scope.featuredPropertys=[];
    $scope.featuredPropertyedit=false;

    Property.query({isSale:1},function(property){
        $scope.properties=property;
    });

    FeaturedProperty.query(function(featuredProperty){
        $scope.featuredPropertys=featuredProperty;
    });

    $scope.newFeaturedProperty = function (argument) {
        $scope.featuredPropertyedit = true;
        $scope.newfeaturedProperty = new FeaturedProperty();
        $scope.curFeaturedProperty = {};
    };
    $scope.editFeaturedProperty = function (thisFeaturedProperty) {
        $scope.featuredPropertyedit = true;
        $scope.curFeaturedProperty =  thisFeaturedProperty;
        $scope.newfeaturedProperty = angular.copy(thisFeaturedProperty);
        $anchorScroll();
    };
    $scope.addFeaturedProperty = function () {

        if ($scope.curFeaturedProperty.id) {
            $scope.newfeaturedProperty.$update(function(featuredProperty){
                angular.extend($scope.curFeaturedProperty, $scope.curFeaturedProperty, featuredProperty);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Updated Successfully');
            });
        } else{
            $scope.newfeaturedProperty.$save(function(featuredProperty){
                $scope.featuredPropertys.push(featuredProperty);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Saved Successfully');
            });
        }
        $scope.featuredPropertyedit = false;
        $scope.newfeaturedProperty = new FeaturedProperty();
    };
    $scope.deleteFeaturedProperty = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.featuredPropertys.indexOf(item);
                $scope.featuredPropertys.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Featured Property Removed Successfully');
            });
        }
    };
    $scope.cancelFeaturedProperty = function () {
        $scope.featuredPropertyedit = false;
        $scope.newfeaturedProperty = new FeaturedProperty();
    };
});
app.controller('GalleryController', function($scope,$http,$window,ngNotify,Gallery){

    $scope.galleries=[];
    Gallery.query(function(gallery){
        $scope.galleries=gallery;
    });

    $scope.galleryedit=false;

    $scope.newGallery = function () {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
    };

    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.addGallery = function () {

        $scope.newgallery.$save(function(gallery){
            $scope.galleries=[];
            Gallery.query(function(gallery){
                $scope.galleries=gallery;
            });
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('Photos Saved Successfully');
            //$window.location.reload();
        });

        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.deletePhoto=function(item){
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.galleries.indexOf(item);
                $scope.galleries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Photo Removed Successfully');

            });
        }
    };
});
app.controller('HomeController', function($scope,$http,$location,UserInfo){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    $scope.name="";


    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
    /* Nav menu */


    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "active" : "";
    };

    $scope.user = {};

    UserInfo.query().success(function(data){
        $scope.user = data;
        console.log(data);
    });

});


app.directive('clickAnywhereButHere', function($document){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function() {
                // magic here.
                scope.$apply(attr.clickAnywhereButHere);
            })
        }
    }
});
app.controller('LatestDealController', function($scope,$http,$anchorScroll,ngNotify,LatestDeal){
    $scope.latestDeals=[];
    $scope.latestDealedit=false;

    LatestDeal.query(function(latestDeal){
        $scope.latestDeals=latestDeal;
    });

    $scope.newLatestDeal = function (argument) {
        $scope.latestDealedit = true;
        $scope.newlatestDeal = new LatestDeal();
        $scope.curLatestDeal = {};
    };
    $scope.editLatestDeal = function (thisLatestDeal) {
        $scope.latestDealedit = true;
        $scope.curLatestDeal =  thisLatestDeal;
        $scope.newlatestDeal = angular.copy(thisLatestDeal);
        $anchorScroll();
    };
    $scope.addLatestDeal = function () {

        if ($scope.curLatestDeal.id) {
            $scope.newlatestDeal.$update(function(latestDeal){
                angular.extend($scope.curLatestDeal, $scope.curLatestDeal, latestDeal);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Updated Successfully');
            });
        } else{
            $scope.newlatestDeal.$save(function(latestDeal){
                $scope.latestDeals.push(latestDeal);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Saved Successfully');
            });
        }
        $scope.latestDealedit = false;
        $scope.newlatestDeal = new LatestDeal();
    };
    $scope.deleteLatestDeal = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.latestDeals.indexOf(item);
                $scope.latestDeals.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Latest Deal Removed Successfully');
            });
        }
    };
    $scope.cancelLatestDeal = function () {
        $scope.latestDealedit = false;
        $scope.newlatestDeal = new LatestDeal();
    };

    $scope.deletePhoto=function(item){
        $http.delete('api/photo/'+item.id).
            success(function (data, status, headers, config) {
                var curIndex = $scope.newlatestDeal.photos.indexOf(item);
                $scope.newlatestDeal.photos.splice(curIndex, 1);
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);

            });
    };
});
app.controller('ProfileController', function($scope,$http,$anchorScroll,ngNotify,User,UserInfo){
    
    $scope.users=[];

    $scope.useredit=false;

    User.query(function(user){
        $scope.users=user;
    });

    $scope.newUser = function (argument) {
        $scope.useredit = true;
        $scope.newuser = new User();
        $scope.curUser = {};
        $scope.newuser.isAdmin=0;
    };
    $scope.editUser = function (thisUser) {
        $scope.useredit = true;
        $scope.curUser =  thisUser;
        $scope.newuser = angular.copy(thisUser);
        $anchorScroll();
    };

    $scope.addUser = function () {

        if ($scope.curUser.id) {
            $scope.newuser.$update(function(User){
                angular.extend($scope.curUser, $scope.curUser, User);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Updated Successfully');
            });
        } else{
            $scope.newuser.$save(function(User){
                $scope.users.push(User);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Saved Successfully....Default Password is admin');
            });
        }
        $scope.useredit = false;
        $scope.newuser = new User();
    };
    $scope.deleteUser = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.users.indexOf(item);
                $scope.users.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Removed Successfully');
            });
        }
    };
    $scope.cancelUser = function () {
        $scope.useredit = false;
        $scope.newuser = new User();
    };
    
    $scope.updateProfile=function(){
        $http.post('/api/updateProfile',{name:$scope.name,email:$scope.email}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 4000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Profile Changed Successfully...Please Re login to Change Profile name');
            });
    };
    

    $scope.changePassword=function(curpassword,newpassword,repassword){
        $http.post('/api/changepassword',{curpassword:curpassword,newpassword:newpassword,repassword:repassword}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 4000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Password Changed Successfully...Please Re login');
            });
    };


    $scope.resetPassword = function (user,ev) {

        var confirmDelete = confirm("Do you really need to Change Permission ?");
        if (confirmDelete) {
            $http.post('/api/resetPassword',{id:user.id}).
                success(function(data,status,headers,config){
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 4000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set(user.name+'\'s Password Reset to admin');
                }).error(function(data,status,headers,config){
                    console.log(data);

                });
        }

    };


    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;
        $scope.name = thisUser.name;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        //$scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
    };

    $scope.savePermission = function (ev){

        var confirmDelete = confirm("Do you really need to Change Permission ?");
        if (confirmDelete) {
            UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
                .success(function(data){
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 4000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Permission Changed Successfully..');
                    $scope.resetPermission=false;
                });
        }

    };

    $scope.editPage = false;
    $scope.writePage= true;
    $scope.deletePage = false;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.toggleDelete = function(page){
        page.delete = page.delete=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        thisPage.write=0;
        thisPage.edit=0;
        thisPage.delete=0;
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };
});
app.controller('PropertyController', function($scope,$http,$anchorScroll,$modal,ngNotify,Property){
    $scope.propertys=[];
    $scope.propertyedit=false;


    $scope.districts=[];
    $scope.getDistrict=function(state_id){
        $http.get('/getDistricts',{params:{state_id:state_id}}).
            success(function(data, status)
            {
                $scope.districts=data;
            });
    };

    $http.get('/getState').
        success(function(data, status)
        {
            $scope.states=data;
        });


    Property.query(function(property){
        $scope.propertys=property;
    });

    $scope.newProperty = function (argument) {
        $scope.propertyedit = true;
        $scope.newproperty = new Property();
        $scope.curProperty = {};
    };
    $scope.editProperty = function (thisProperty) {
        $scope.propertyedit = true;
        $scope.curProperty =  thisProperty;
        $scope.newproperty = angular.copy(thisProperty);
        $anchorScroll();
    };

    $scope.addProperty = function () {

        if ($scope.curProperty.id) {
            $scope.newproperty.$update(function(property){
                angular.extend($scope.curProperty, $scope.curProperty, property);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Updated Successfully');
            });
        } else{
            $scope.newproperty.$save(function(property){
                $scope.propertys.push(property);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Saved Successfully');
            });
        }
        $scope.propertyedit = false;
        $scope.newproperty = new Property();
    };
    $scope.deleteProperty = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertys.indexOf(item);
                $scope.propertys.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Removed Successfully');
            });
        }
    };
    $scope.cancelProperty = function () {
        $scope.propertyedit = false;
        $scope.newproperty = new Property();
    };


    $scope.viewProperty = function (p) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproperty',
            controller:'ViewPropertyController',
            size: 'lg',
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

    $scope.deletePhoto=function(item){
        $http.delete('api/photo/'+item.id).
            success(function (data, status, headers, config) {
                var curIndex = $scope.newproperty.photos.indexOf(item);
                $scope.newproperty.photos.splice(curIndex, 1);
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

});

app.controller('ViewPropertyController', function ($scope, $modalInstance,$modal, item) {

    $scope.property = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

});

app.controller('PropertyTypeController', function($scope,$http,$rootScope,$timeout,$anchorScroll,ngNotify,PropertyType){

    $scope.loading = true;
    $rootScope.$on('$routeChangeSuccess', function () {
        console.log('$routeChangeSuccess');
        //hide loading gif
        $timeout(function(){
            $scope.loading = false;
        }, 2000);
    });


    $scope.propertyTypes=[];
    $scope.propertyTypeedit=false;

    PropertyType.query(function(propertyType){
        $scope.propertyTypes=propertyType;
    });

    $scope.newPropertyType = function (argument) {
        $scope.propertyTypeedit = true;
        $scope.newpropertyType = new PropertyType();
        $scope.curPropertyType = {};
    };
    $scope.editPropertyType = function (thisPropertyType) {
        $scope.propertyTypeedit = true;
        $scope.curPropertyType =  thisPropertyType;
        $scope.newpropertyType = angular.copy(thisPropertyType);
        $anchorScroll();
    };
    $scope.addPropertyType = function () {

        if ($scope.curPropertyType.id) {
            $scope.newpropertyType.$update(function(propertyType){
                angular.extend($scope.curPropertyType, $scope.curPropertyType, propertyType);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Updated Successfully');
            });
        } else{
            $scope.newpropertyType.$save(function(propertyType){
                $scope.propertyTypes.push(propertyType);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Saved Successfully');
            });
        }
        $scope.propertyTypeedit = false;
        $scope.newpropertyType = new PropertyType();
    };
    $scope.deletePropertyType = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertyTypes.indexOf(item);
                $scope.propertyTypes.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Removed Successfully');
            });
        }
    };
    $scope.cancelPropertyType = function () {
        $scope.propertyTypeedit = false;
        $scope.newpropertyType = new PropertyType();
    };
});
app.controller('PropertyFeatureController', function($scope,$http,$rootScope,$timeout,$anchorScroll,ngNotify,PropertyFeature){

    $scope.loading = true;
    $rootScope.$on('$routeChangeSuccess', function () {
        console.log('$routeChangeSuccess');
        //hide loading gif
        $timeout(function(){
            $scope.loading = false;
        }, 2000);
    });


    $scope.propertyFeatures=[];
    $scope.propertyFeatureedit=false;

    PropertyFeature.query(function(propertyFeature){
       $scope.propertyFeatures=propertyFeature;
    });

    $scope.newPropertyFeature = function (argument) {
        $scope.propertyFeatureedit = true;
        $scope.newpropertyFeature = new PropertyFeature();
        $scope.curPropertyFeature = {};
    };
    $scope.editPropertyFeature = function (thisPropertyFeature) {
        $scope.propertyFeatureedit = true;
        $scope.curPropertyFeature =  thisPropertyFeature;
        $scope.newpropertyFeature = angular.copy(thisPropertyFeature);
        $anchorScroll();
    };
    $scope.addPropertyFeature = function () {

        if ($scope.curPropertyFeature.id) {
            $scope.newpropertyFeature.$update(function(propertyFeature){
                angular.extend($scope.curPropertyFeature, $scope.curPropertyFeature, propertyFeature);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Updated Successfully');
            });
        } else{
            $scope.newpropertyFeature.$save(function(propertyFeature){
                $scope.propertyFeatures.push(propertyFeature);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Saved Successfully');
            });
        }
        $scope.propertyFeatureedit = false;
        $scope.newpropertyFeature = new PropertyFeature();
    };
    $scope.deletePropertyFeature = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.propertyFeatures.indexOf(item);
                $scope.propertyFeatures.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Property Type Removed Successfully');
            });
        }
    };
    $scope.cancelPropertyFeature = function () {
        $scope.propertyFeatureedit = false;
        $scope.newpropertyFeature = new PropertyFeature();
    };
});
app.controller('TestimonialController', function($scope,$http,$anchorScroll,ngNotify,Testimonial){
    $scope.testimonials=[];
    $scope.testimonialedit=false;

    Testimonial.query(function(testimonial){
        $scope.testimonials=testimonial;
    });

    $scope.newTestimonial = function (argument) {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisTestimonial) {
        $scope.testimonialedit = true;
        $scope.curTestimonial =  thisTestimonial;
        $scope.newtestimonial = angular.copy(thisTestimonial);
        $anchorScroll();
    };
    $scope.addTestimonial = function () {

        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function(testimonial){
                angular.extend($scope.curTestimonial, $scope.curTestimonial, testimonial);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });
        } else{
            $scope.newtestimonial.$save(function(testimonial){
                $scope.testimonials.push(testimonial);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };
    $scope.deleteTestimonial = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.testimonials.indexOf(item);
                $scope.testimonials.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Removed Successfully');
            });
        }
    };
    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };
});
//# sourceMappingURL=myapp.js.map
