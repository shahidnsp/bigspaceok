var app = angular.
    module('myWeb', [
    ]);

app.controller('HomeController', function($scope,$http){
    $scope.districts=[];
    $scope.getDistrict=function(state_id){
        $http.get('/getDistricts',{params:{state_id:state_id}}).
            success(function(data, status)
            {
                $scope.districts=data;
            });
    };
});

app.controller('FormController', function($scope,$http){
    $scope.districts=[];
    $scope.getDistrict=function(state_id){
        $http.get('/getDistricts',{params:{state_id:state_id}}).
            success(function(data, status)
            {
                $scope.districts=data;
            });
    };
});